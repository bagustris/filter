Test for Bad_Good
To import-> Hits: 2093, Tracks: 358 Events: 1
To bad->good: Hits: 2093, Tracks: 358 Events: 1
Filter with Bad_Good method
Time CPU total Bad_Good Events: 1 Tracks: 358 Hits: 2093 = 1.032352e-04 s

Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.568794e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.105600e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.820129e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.218721e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.173120e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.120567e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.876671e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 5.507469e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 4.7456e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.41482e-05 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.626820e-01 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 5.4976e-05 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 8.39233e-05 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.504220e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OMP method
Time CPU total OMP Events: 1 Tracks: 358 Hits: 2093 = 6.861687e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 3158, Tracks: 532 Events: 2
To bad->good: Hits: 3158, Tracks: 532 Events: 2
Filter with Bad_Good method
Time CPU total Bad_Good Events: 2 Tracks: 532 Hits: 3158 = 1.480579e-04 s

Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 2.040863e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 8.297600e-05 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 7.820129e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 6.909721e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.125760e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.091957e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 6.968529e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 8.296967e-05 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 4.5632e-05 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.39098e-05 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.443268e-01 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 5.7824e-05 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 8.79765e-05 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 7.017672e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OMP method
Time CPU total OMP Events: 2 Tracks: 532 Hits: 3158 = 7.541180e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 4741, Tracks: 779 Events: 3
To bad->good: Hits: 4741, Tracks: 779 Events: 3
Filter with Bad_Good method
Time CPU total Bad_Good Events: 3 Tracks: 779 Hits: 4741 = 2.341270e-04 s

Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 2.949238e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.169601e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.915497e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 6.509871e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.128000e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.091957e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 6.844110e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.239777e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 4.7552e-05 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 7.39098e-05 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 7.193351e-01 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 5.472e-05 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 8.2016e-05 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 7.236910e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OMP method
Time CPU total OMP Events: 3 Tracks: 779 Hits: 4741 = 7.491112e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
To bad->good: Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad_Good method
Time CPU total Bad_Good Events: 4 Tracks: 1300 Hits: 8061 = 4.227161e-04 s

Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 5.288124e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.585600e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.201599e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 6.879280e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.159360e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.118183e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 6.833811e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.179146e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 4.8352e-05 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 7.79629e-05 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 7.285490e-01 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 5.6224e-05 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 8.29697e-05 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 7.210040e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OMP method
Time CPU total OMP Events: 4 Tracks: 1300 Hits: 8061 = 7.438660e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
To bad->good: Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad_Good method
Time CPU total Bad_Good Events: 5 Tracks: 1914 Hits: 11850 = 5.872250e-04 s

Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 7.750988e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.736001e-05 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.296967e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 6.874118e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.160000e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.118183e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 6.921730e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.108978e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 5.4112e-05 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 8.2016e-05 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 7.289362e-01 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 6.0384e-05 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 8.79765e-05 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 7.289579e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OMP method
Time CPU total OMP Events: 5 Tracks: 1914 Hits: 11850 = 8.361340e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
To bad->good: Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad_Good method
Time CPU total Bad_Good Events: 6 Tracks: 2089 Hits: 12849 = 6.999969e-04 s

Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 8.199215e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 9.161600e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.797646e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 6.885400e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.186240e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.139641e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 6.898971e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 3.440380e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 5.5744e-05 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 8.29697e-05 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 7.265768e-01 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 6.0768e-05 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 9.01222e-05 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 7.293360e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OMP method
Time CPU total OMP Events: 6 Tracks: 2089 Hits: 12849 = 7.870197e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
To bad->good: Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad_Good method
Time CPU total Bad_Good Events: 7 Tracks: 2418 Hits: 14812 = 7.872581e-04 s

Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 8.969307e-04 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.120000e-05 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 8.797646e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 6.631980e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.264320e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.230240e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 6.526589e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 3.890991e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 5.8592e-05 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 8.51154e-05 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 6.839809e-01 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.7904e-05 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 9.70364e-05 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.860471e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OMP method
Time CPU total OMP Events: 7 Tracks: 2418 Hits: 14812 = 1.580000e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
To bad->good: Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad_Good method
Time CPU total Bad_Good Events: 8 Tracks: 2586 Hits: 15814 = 8.251667e-04 s

Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 9.009838e-04 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.011200e-04 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.799004e-05 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.511719e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.363520e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.299381e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 6.490850e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 4.150867e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 6.1984e-05 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 9.29832e-05 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 6.892641e-01 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 6.4992e-05 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 9.20296e-05 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 6.877439e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OMP method
Time CPU total OMP Events: 8 Tracks: 2586 Hits: 15814 = 8.089542e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
To bad->good: Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad_Good method
Time CPU total Bad_Good Events: 9 Tracks: 2764 Hits: 16842 = 8.978844e-04 s

Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 8.440018e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.119680e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.060963e-04 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.578929e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.384000e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.308918e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 6.515429e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 4.398823e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.384e-05 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 9.39369e-05 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.927512e-01 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 6.7936e-05 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 9.60827e-05 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 6.837490e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OMP method
Time CPU total OMP Events: 9 Tracks: 2764 Hits: 16842 = 8.039474e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
To bad->good: Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad_Good method
Time CPU total Bad_Good Events: 10 Tracks: 3094 Hits: 18940 = 8.959770e-04 s

Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 1.080990e-03 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.074240e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.029968e-04 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 6.531339e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.324800e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.270771e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 6.571651e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 4.999638e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.6368e-05 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 9.20296e-05 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.885579e-01 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 6.5536e-05 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 9.29832e-05 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 7.126539e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OMP method
Time CPU total OMP Events: 10 Tracks: 3094 Hits: 18940 = 8.099079e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
To bad->good: Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad_Good method
Time CPU total Bad_Good Events: 11 Tracks: 3613 Hits: 22010 = 1.020193e-03 s

Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.222134e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.105280e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.070499e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.728508e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.496320e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.418591e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.539099e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 6.170273e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.5424e-05 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000102997 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.245030e-01 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.376e-05 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000102997 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 6.987770e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OMP method
Time CPU total OMP Events: 11 Tracks: 3613 Hits: 22010 = 8.010864e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
To bad->good: Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad_Good method
Time CPU total Bad_Good Events: 12 Tracks: 3745 Hits: 22930 = 1.125097e-03 s

Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.332998e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.112640e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.080036e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 6.755300e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.398720e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.339912e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 6.569369e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 6.089211e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 7.6768e-05 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000102997 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 7.005000e-01 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 7.4176e-05 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000100136 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 6.896751e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OMP method
Time CPU total OMP Events: 12 Tracks: 3745 Hits: 22930 = 8.308887e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
To bad->good: Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad_Good method
Time CPU total Bad_Good Events: 13 Tracks: 4414 Hits: 27034 = 1.392126e-03 s

Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 1.567125e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.289600e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.230240e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 6.517811e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.468480e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.440048e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 6.497891e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 6.790161e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 8.4992e-05 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.00011611 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 6.881540e-01 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 8.3104e-05 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000111103 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 6.830270e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OMP method
Time CPU total OMP Events: 13 Tracks: 4414 Hits: 27034 = 8.468628e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
To bad->good: Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad_Good method
Time CPU total Bad_Good Events: 14 Tracks: 4587 Hits: 28064 = 1.435995e-03 s

Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 1.709938e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.226240e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.189709e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 6.536489e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.575360e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.499653e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 6.495910e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 7.550716e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 8.5632e-05 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.00011611 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 6.985879e-01 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 8.2176e-05 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000109911 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 6.985390e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OMP method
Time CPU total OMP Events: 14 Tracks: 4587 Hits: 28064 = 8.089542e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
To bad->good: Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad_Good method
Time CPU total Bad_Good Events: 15 Tracks: 5044 Hits: 30895 = 1.655817e-03 s

Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 2.006054e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.300480e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.261234e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 6.554089e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.570560e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.518726e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 6.530850e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 8.192062e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 9.0848e-05 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000119209 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 6.890240e-01 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 9.152e-05 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000119209 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 6.967618e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OMP method
Time CPU total OMP Events: 15 Tracks: 5044 Hits: 30895 = 8.838177e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
To bad->good: Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad_Good method
Time CPU total Bad_Good Events: 16 Tracks: 5141 Hits: 31449 = 1.639128e-03 s

Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 1.761913e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.301440e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.261234e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.580579e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.534400e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.490116e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 6.573269e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 8.192062e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 9.4336e-05 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.00012207 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 6.922400e-01 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 9.072e-05 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000118017 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 6.923490e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OMP method
Time CPU total OMP Events: 16 Tracks: 5141 Hits: 31449 = 1.614094e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
To bad->good: Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad_Good method
Time CPU total Bad_Good Events: 17 Tracks: 5353 Hits: 32810 = 1.403093e-03 s

Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.660109e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.447360e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.399517e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 6.560931e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.678080e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.611710e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 6.573560e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 8.862019e-04 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 9.2064e-05 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000119925 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 7.023101e-01 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 9.28e-05 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000119925 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 6.995292e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OMP method
Time CPU total OMP Events: 17 Tracks: 5353 Hits: 32810 = 8.730888e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
To bad->good: Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad_Good method
Time CPU total Bad_Good Events: 18 Tracks: 5750 Hits: 35226 = 1.733065e-03 s

Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 1.675129e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.422400e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.358986e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 6.598101e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.670080e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.611710e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.523800e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 9.238720e-04 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000102656 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000252962 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 6.954510e-01 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 9.7568e-05 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000252962 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 6.930091e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OMP method
Time CPU total OMP Events: 18 Tracks: 5750 Hits: 35226 = 8.308887e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
To bad->good: Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad_Good method
Time CPU total Bad_Good Events: 19 Tracks: 6260 Hits: 38329 = 1.902103e-03 s

Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 1.821995e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.429440e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.380444e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 6.546721e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.664960e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.609325e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 6.511300e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.003027e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000101248 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000266075 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 6.975129e-01 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000105952 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000374794 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 6.896780e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OMP method
Time CPU total OMP Events: 19 Tracks: 6260 Hits: 38329 = 8.389950e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
To bad->good: Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad_Good method
Time CPU total Bad_Good Events: 20 Tracks: 6560 Hits: 40004 = 1.915932e-03 s

Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 2.207041e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.451200e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.378059e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 6.555910e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.704320e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.640320e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 6.858141e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.116037e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.00010384 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000252962 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 6.858499e-01 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000106496 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000254154 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 6.873131e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OMP method
Time CPU total OMP Events: 20 Tracks: 6560 Hits: 40004 = 1.646042e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
To bad->good: Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad_Good method
Time CPU total Bad_Good Events: 30 Tracks: 9310 Hits: 57158 = 2.893925e-03 s

Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.395081e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.780800e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.740456e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.587780e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.149120e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.100468e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 6.689219e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.540184e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.00013504 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000163078 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 6.934681e-01 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.00012704 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000157118 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 6.868830e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OMP method
Time CPU total OMP Events: 30 Tracks: 9310 Hits: 57158 = 8.561611e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
To bad->good: Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad_Good method
Time CPU total Bad_Good Events: 40 Tracks: 12174 Hits: 75953 = 3.684998e-03 s

Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 3.754854e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.068480e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.019405e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 6.560740e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.532800e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.460480e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 6.630509e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 2.004862e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000179392 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000216007 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 7.078381e-01 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000157472 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.00019598 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 6.907840e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OMP method
Time CPU total OMP Events: 40 Tracks: 12174 Hits: 75953 = 9.000301e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
To bad->good: Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad_Good method
Time CPU total Bad_Good Events: 50 Tracks: 15024 Hits: 93663 = 4.374027e-03 s

Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 4.561901e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.178880e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.160072e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 6.526051e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.824000e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.801418e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.517880e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.562046e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000198752 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000226021 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 6.961119e-01 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.00017264 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000203848 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 6.973910e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OMP method
Time CPU total OMP Events: 50 Tracks: 15024 Hits: 93663 = 1.810074e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
To bad->good: Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad_Good method
Time CPU total Bad_Good Events: 60 Tracks: 18356 Hits: 114400 = 5.401850e-03 s

Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 6.567955e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.527680e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.510548e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.561930e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.147840e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.139973e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 6.539478e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 2.998114e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000240096 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000396967 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 6.967208e-01 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000200224 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000344992 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 6.896131e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OMP method
Time CPU total OMP Events: 60 Tracks: 18356 Hits: 114400 = 1.000881e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
To bad->good: Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad_Good method
Time CPU total Bad_Good Events: 70 Tracks: 21691 Hits: 135212 = 6.402016e-03 s

Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 6.476879e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.754880e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.748966e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 6.576481e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.413120e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.421307e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.620550e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 3.621101e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.0002704 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000429153 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 6.967990e-01 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000219712 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000377178 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 6.966939e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OMP method
Time CPU total OMP Events: 70 Tracks: 21691 Hits: 135212 = 1.073122e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
To bad->good: Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad_Good method
Time CPU total Bad_Good Events: 80 Tracks: 24967 Hits: 155915 = 7.106066e-03 s

Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 7.416964e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.120960e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.139973e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.749821e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.901120e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.910065e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.579010e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.231930e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.0002992 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.00049901 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 7.006371e-01 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000250464 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000405788 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 6.902070e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OMP method
Time CPU total OMP Events: 80 Tracks: 24967 Hits: 155915 = 1.055002e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
To bad->good: Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad_Good method
Time CPU total Bad_Good Events: 90 Tracks: 28121 Hits: 175352 = 8.852005e-03 s

Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 9.526968e-03 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.297280e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.321171e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 6.686990e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.379520e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.339218e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 6.601090e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 4.603863e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000335008 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000492811 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 7.104399e-01 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000268352 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000413895 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 7.003181e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OMP method
Time CPU total OMP Events: 90 Tracks: 28121 Hits: 175352 = 1.881123e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
To bad->good: Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad_Good method
Time CPU total Bad_Good Events: 100 Tracks: 31506 Hits: 196714 = 9.667873e-03 s

Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 1.084089e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.660800e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.678799e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.585300e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.711360e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.739761e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.647198e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 5.158901e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.00035648 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000528097 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 6.961370e-01 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000298048 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000456095 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 7.070842e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OMP method
Time CPU total OMP Events: 100 Tracks: 31506 Hits: 196714 = 1.182079e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
To bad->good: Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad_Good method
Time CPU total Bad_Good Events: 110 Tracks: 34413 Hits: 214801 = 1.060390e-02 s

Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.189995e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.833280e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.869534e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.669819e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 4.968960e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.009174e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.525280e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 5.645990e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000407136 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000567913 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 6.968820e-01 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.00031392 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000466108 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 6.908760e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OMP method
Time CPU total OMP Events: 110 Tracks: 34413 Hits: 214801 = 1.164913e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
To bad->good: Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad_Good method
Time CPU total Bad_Good Events: 120 Tracks: 37459 Hits: 233881 = 1.237988e-02 s

Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.330996e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.194240e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.251003e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.598091e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.486400e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.528927e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.599009e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 6.086111e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000434528 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000594854 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 7.055149e-01 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000354368 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000556946 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 6.982021e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OMP method
Time CPU total OMP Events: 120 Tracks: 37459 Hits: 233881 = 1.259804e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
To bad->good: Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad_Good method
Time CPU total Bad_Good Events: 130 Tracks: 40754 Hits: 254225 = 1.147103e-02 s

Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.185203e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.427200e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.491806e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 6.671641e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.792640e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.857944e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 6.584330e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 6.704092e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000481824 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000638962 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 7.018120e-01 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.0003728 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000545979 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 7.079480e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OMP method
Time CPU total OMP Events: 130 Tracks: 40754 Hits: 254225 = 1.281023e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
To bad->good: Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad_Good method
Time CPU total Bad_Good Events: 140 Tracks: 44705 Hits: 278857 = 1.282907e-02 s

Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.319981e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.789120e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.858971e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 6.672199e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.332161e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.411076e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.651769e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 7.319927e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.00052432 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.00067091 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 7.056870e-01 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000402176 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000550032 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 7.021630e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OMP method
Time CPU total OMP Events: 140 Tracks: 44705 Hits: 278857 = 1.283884e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
To bad->good: Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad_Good method
Time CPU total Bad_Good Events: 150 Tracks: 48106 Hits: 299938 = 1.379299e-02 s

Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.342297e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.065600e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.140305e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 6.862168e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.672320e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.749630e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.604280e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 8.037090e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000557664 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000720024 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 6.992590e-01 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000429056 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.00058794 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 9.264600e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OMP method
Time CPU total OMP Events: 150 Tracks: 48106 Hits: 299938 = 1.322985e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
To bad->good: Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad_Good method
Time CPU total Bad_Good Events: 160 Tracks: 51627 Hits: 321407 = 1.527619e-02 s

Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.517010e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.488320e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.531311e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 6.579168e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.132160e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.219315e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.585999e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 8.225918e-03 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000582112 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000729084 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 7.004480e-01 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000455552 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.00060606 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 6.952550e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OMP method
Time CPU total OMP Events: 160 Tracks: 51627 Hits: 321407 = 1.296997e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
To bad->good: Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad_Good method
Time CPU total Bad_Good Events: 170 Tracks: 54445 Hits: 339287 = 1.577187e-02 s

Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.736903e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.626880e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.710125e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 6.630151e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.372801e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.469654e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.578741e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 9.117126e-03 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000604352 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000755072 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 7.032099e-01 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.00048128 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000634909 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 7.022901e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OMP method
Time CPU total OMP Events: 170 Tracks: 54445 Hits: 339287 = 1.304865e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
To bad->good: Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad_Good method
Time CPU total Bad_Good Events: 180 Tracks: 57335 Hits: 357414 = 1.669025e-02 s

Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 1.754403e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.871680e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.979538e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.680279e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.858240e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.958412e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.690340e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 9.339809e-03 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000664544 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000864029 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 7.153330e-01 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000497344 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000653982 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 7.029240e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OMP method
Time CPU total OMP Events: 180 Tracks: 57335 Hits: 357414 = 1.300097e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
To bad->good: Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad_Good method
Time CPU total Bad_Good Events: 190 Tracks: 60572 Hits: 377316 = 1.730704e-02 s

Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 2.081895e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.150720e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.239414e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.640809e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.106561e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.230209e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 6.954410e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 1.001596e-02 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000662528 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000813007 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 7.494161e-01 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000525856 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000684023 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 7.348039e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OMP method
Time CPU total OMP Events: 190 Tracks: 60572 Hits: 377316 = 2.631903e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
To bad->good: Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad_Good method
Time CPU total Bad_Good Events: 200 Tracks: 63921 Hits: 398354 = 1.861596e-02 s

Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 2.029991e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.534080e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.630421e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.728032e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.582721e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.680820e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 6.683679e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.052189e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000726656 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000928164 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 6.988840e-01 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000553216 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000709057 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 6.981971e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OMP method
Time CPU total OMP Events: 200 Tracks: 63921 Hits: 398354 = 1.466990e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
To bad->good: Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad_Good method
Time CPU total Bad_Good Events: 210 Tracks: 66802 Hits: 416406 = 2.222371e-02 s

Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.219987e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.739200e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.890297e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.767941e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.036480e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.179115e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 6.677711e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.061988e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000755968 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000908136 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 7.017589e-01 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000581248 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000739098 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 7.014410e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OMP method
Time CPU total OMP Events: 210 Tracks: 66802 Hits: 416406 = 1.413822e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
To bad->good: Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad_Good method
Time CPU total Bad_Good Events: 220 Tracks: 69786 Hits: 435132 = 2.067065e-02 s

Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.459097e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.048320e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.178783e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 6.677051e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.231041e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.381771e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 6.609051e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.090407e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.00080784 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000963926 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 7.145920e-01 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000600256 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000770092 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 7.184141e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OMP method
Time CPU total OMP Events: 220 Tracks: 69786 Hits: 435132 = 1.438141e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
To bad->good: Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad_Good method
Time CPU total Bad_Good Events: 230 Tracks: 72547 Hits: 452586 = 2.145410e-02 s

Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.626801e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.340160e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.491112e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 6.649308e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.756481e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.911060e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 6.577859e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.142001e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000815744 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000971079 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 6.956360e-01 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.00063008 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.000820875 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 6.989069e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OMP method
Time CPU total OMP Events: 230 Tracks: 72547 Hits: 452586 = 2.344131e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
To bad->good: Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad_Good method
Time CPU total Bad_Good Events: 240 Tracks: 76144 Hits: 474872 = 2.439523e-02 s

Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 2.754092e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.557120e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.710457e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 6.672418e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.001824e-03 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.018047e-03 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 6.771600e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.203084e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000862528 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.00101495 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 7.178080e-01 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.000650304 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.000802994 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 7.033830e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OMP method
Time CPU total OMP Events: 240 Tracks: 76144 Hits: 474872 = 2.461195e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
To bad->good: Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad_Good method
Time CPU total Bad_Good Events: 250 Tracks: 79172 Hits: 493810 = 2.432489e-02 s

Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 2.621102e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.736640e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.889271e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 6.670470e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.030592e-03 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.048088e-03 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 6.726320e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.250792e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.000874976 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00102997 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 7.269208e-01 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000664768 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000813961 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 7.119191e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OMP method
Time CPU total OMP Events: 250 Tracks: 79172 Hits: 493810 = 1.522064e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
To bad->good: Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad_Good method
Time CPU total Bad_Good Events: 260 Tracks: 82444 Hits: 514314 = 2.339411e-02 s

Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.276707e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.077440e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.270741e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 6.915200e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.070400e-03 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.089096e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 6.731761e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.338196e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00094144 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.001091 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 6.989088e-01 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000701472 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000850916 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 7.047050e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OMP method
Time CPU total OMP Events: 260 Tracks: 82444 Hits: 514314 = 2.141953e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
To bad->good: Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad_Good method
Time CPU total Bad_Good Events: 270 Tracks: 85661 Hits: 534537 = 2.394795e-02 s

Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 2.340913e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.370560e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.549690e-04 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 6.751342e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.121824e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.139879e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 6.623261e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.366091e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00098256 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00114107 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 7.082372e-01 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000725152 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000885963 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 6.995411e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OMP method
Time CPU total OMP Events: 270 Tracks: 85661 Hits: 534537 = 2.093792e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
To bad->good: Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad_Good method
Time CPU total Bad_Good Events: 280 Tracks: 89234 Hits: 557160 = 2.610493e-02 s

Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 2.701020e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.739840e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.950233e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 6.665721e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.164224e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.183987e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.655021e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.403522e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.000978208 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00113416 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 7.311590e-01 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00074816 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.000895023 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 6.968050e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OMP method
Time CPU total OMP Events: 280 Tracks: 89234 Hits: 557160 = 1.542091e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
To bad->good: Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad_Good method
Time CPU total Bad_Good Events: 290 Tracks: 92347 Hits: 576611 = 2.655077e-02 s

Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 2.563596e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.869760e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 9.109974e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 6.622498e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.189056e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.210928e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 6.660450e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.482916e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00102643 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00118399 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 6.987920e-01 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000773376 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000914812 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 6.939349e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OMP method
Time CPU total OMP Events: 290 Tracks: 92347 Hits: 576611 = 2.218962e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
To bad->good: Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad_Good method
Time CPU total Bad_Good Events: 300 Tracks: 95876 Hits: 598963 = 2.730823e-02 s

Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 2.951193e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.250240e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.489059e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 6.657190e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.242208e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.266003e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 6.614101e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.516294e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.0010704 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00122404 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 7.112172e-01 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000802816 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000949144 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 7.009549e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OMP method
Time CPU total OMP Events: 300 Tracks: 95876 Hits: 598963 = 2.487898e-03 s

-----------------------------
