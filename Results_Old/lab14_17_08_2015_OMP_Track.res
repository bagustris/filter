Test for Bad_Good
To import-> Hits: 2093, Tracks: 358 Events: 1
To bad->good: Hits: 2093, Tracks: 358 Events: 1
Filter with Bad_Good method
Time CPU total Bad_Good Events: 1 Tracks: 358 Hits: 2093 = 1.008511e-04 s

Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.428127e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.286401e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.081032e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 6.905539e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.053440e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.020432e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.710260e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 5.507469e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 4.5216e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.10487e-05 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.173550e-01 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 5.0656e-05 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.82013e-05 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.179458e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OMP method
Time CPU total OMP Events: 1 Tracks: 358 Hits: 2093 = 7.500648e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 3158, Tracks: 532 Events: 2
To bad->good: Hits: 3158, Tracks: 532 Events: 2
Filter with Bad_Good method
Time CPU total Bad_Good Events: 2 Tracks: 532 Hits: 3158 = 1.521111e-04 s

Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 1.859665e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 9.830401e-05 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 9.608269e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 6.521881e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.042880e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.020432e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 6.679659e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 8.296967e-05 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 4.4672e-05 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.00951e-05 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.019560e-01 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 5.2704e-05 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 8.2016e-05 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 6.922500e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OMP method
Time CPU total OMP Events: 2 Tracks: 532 Hits: 3158 = 7.569790e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 4741, Tracks: 779 Events: 3
To bad->good: Hits: 4741, Tracks: 779 Events: 3
Filter with Bad_Good method
Time CPU total Bad_Good Events: 3 Tracks: 779 Hits: 4741 = 2.372265e-04 s

Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 2.758503e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.308800e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.200241e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 6.537399e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.140800e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.120567e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 6.516030e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.249313e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 4.5376e-05 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 7.20024e-05 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 6.831279e-01 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 5.6064e-05 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 8.29697e-05 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 6.849411e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OMP method
Time CPU total OMP Events: 3 Tracks: 779 Hits: 4741 = 8.950233e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
To bad->good: Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad_Good method
Time CPU total Bad_Good Events: 4 Tracks: 1300 Hits: 8061 = 4.379749e-04 s

Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 5.929470e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.732801e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.296967e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 6.469240e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.116160e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.080036e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 6.475260e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.129078e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 5.6256e-05 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 8.29697e-05 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 6.853220e-01 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 5.5936e-05 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 8.89301e-05 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 6.815350e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OMP method
Time CPU total OMP Events: 4 Tracks: 1300 Hits: 8061 = 9.820461e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
To bad->good: Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad_Good method
Time CPU total Bad_Good Events: 5 Tracks: 1914 Hits: 11850 = 5.860329e-04 s

Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 7.691383e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 9.782401e-05 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 9.417534e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 6.454730e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.119360e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.091957e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 6.472189e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.111362e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 5.5264e-05 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 8.2016e-05 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 6.788030e-01 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 6.0448e-05 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 8.98838e-05 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 6.799719e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OMP method
Time CPU total OMP Events: 5 Tracks: 1914 Hits: 11850 = 1.066923e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
To bad->good: Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad_Good method
Time CPU total Bad_Good Events: 6 Tracks: 2089 Hits: 12849 = 6.616116e-04 s

Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 8.728504e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.435200e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.201599e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 6.460690e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.149440e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.120567e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 6.475871e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 3.380775e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 5.4816e-05 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 8.39233e-05 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 6.856639e-01 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 6.0224e-05 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 9.10759e-05 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 6.902020e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OMP method
Time CPU total OMP Events: 6 Tracks: 2089 Hits: 12849 = 1.095057e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
To bad->good: Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad_Good method
Time CPU total Bad_Good Events: 7 Tracks: 2418 Hits: 14812 = 7.660389e-04 s

Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 9.410381e-04 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.577601e-05 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.202957e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 6.480620e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.244800e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.220703e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 6.571929e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 3.919601e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 5.9488e-05 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 8.70228e-05 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 6.859670e-01 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.8512e-05 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 9.89437e-05 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.962812e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OMP method
Time CPU total OMP Events: 7 Tracks: 2418 Hits: 14812 = 1.334906e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
To bad->good: Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad_Good method
Time CPU total Bad_Good Events: 8 Tracks: 2586 Hits: 15814 = 8.511543e-04 s

Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 1.025200e-03 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.577601e-05 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.202957e-05 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.479321e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.248320e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.211166e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 6.477571e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 4.148483e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 5.9488e-05 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 8.98838e-05 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 6.834219e-01 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 6.688e-05 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.00010705 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 7.086949e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OMP method
Time CPU total OMP Events: 8 Tracks: 2586 Hits: 15814 = 1.413822e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
To bad->good: Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad_Good method
Time CPU total Bad_Good Events: 9 Tracks: 2764 Hits: 16842 = 8.268356e-04 s

Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 8.480549e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.009600e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 9.584427e-05 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.478500e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.285120e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.239777e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 6.470611e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 4.429817e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.5344e-05 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 9.48906e-05 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.797581e-01 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 6.544e-05 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 9.29832e-05 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 6.801541e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OMP method
Time CPU total OMP Events: 9 Tracks: 2764 Hits: 16842 = 1.256943e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
To bad->good: Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad_Good method
Time CPU total Bad_Good Events: 10 Tracks: 3094 Hits: 18940 = 9.160042e-04 s

Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 1.004934e-03 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.009920e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 9.894371e-05 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 6.462100e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.265920e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.220703e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 6.481321e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 4.940033e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.7808e-05 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 9.60827e-05 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.825402e-01 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 6.6944e-05 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 9.48906e-05 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 7.008669e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OMP method
Time CPU total OMP Events: 10 Tracks: 3094 Hits: 18940 = 1.707792e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
To bad->good: Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad_Good method
Time CPU total Bad_Good Events: 11 Tracks: 3613 Hits: 22010 = 1.065969e-03 s

Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.321077e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.107840e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.070499e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.469510e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.326720e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.289845e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.476860e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 5.629063e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.5936e-05 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000103951 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 6.807461e-01 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.712e-05 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000108004 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 6.870279e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OMP method
Time CPU total OMP Events: 11 Tracks: 3613 Hits: 22010 = 1.751900e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
To bad->good: Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad_Good method
Time CPU total Bad_Good Events: 12 Tracks: 3745 Hits: 22930 = 1.147032e-03 s

Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.361132e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.184000e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.130104e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 6.519961e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.464000e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.401901e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 6.510448e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 6.029606e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 7.184e-05 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 9.67979e-05 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 6.799948e-01 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 7.9232e-05 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000111818 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 6.891952e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OMP method
Time CPU total OMP Events: 12 Tracks: 3745 Hits: 22930 = 1.792908e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
To bad->good: Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad_Good method
Time CPU total Bad_Good Events: 13 Tracks: 4414 Hits: 27034 = 1.396894e-03 s

Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 1.673937e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.263680e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.220703e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 6.472239e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.410240e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.370907e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 6.483979e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 7.140636e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 9.184e-05 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000120878 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 6.875441e-01 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 8.3744e-05 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000108957 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 6.848140e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OMP method
Time CPU total OMP Events: 13 Tracks: 4414 Hits: 27034 = 1.988888e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
To bad->good: Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad_Good method
Time CPU total Bad_Good Events: 14 Tracks: 4587 Hits: 28064 = 1.462936e-03 s

Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 1.791000e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.287040e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.251698e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 6.499319e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.433280e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.389980e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 6.474280e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 7.410049e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 8.7168e-05 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.00011611 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 6.830401e-01 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 8.5376e-05 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000112057 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 6.840992e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OMP method
Time CPU total OMP Events: 14 Tracks: 4587 Hits: 28064 = 2.007961e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
To bad->good: Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad_Good method
Time CPU total Bad_Good Events: 15 Tracks: 5044 Hits: 30895 = 1.651049e-03 s

Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 1.887083e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.390400e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.330376e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 6.481509e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.585280e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.530647e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 6.463439e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 8.227825e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 9.0208e-05 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000118017 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 6.813941e-01 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 9.3888e-05 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000129938 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 6.945550e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OMP method
Time CPU total OMP Events: 15 Tracks: 5044 Hits: 30895 = 1.839161e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
To bad->good: Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad_Good method
Time CPU total Bad_Good Events: 16 Tracks: 5141 Hits: 31449 = 1.641035e-03 s

Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 2.569199e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.401920e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.339912e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.720068e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.521280e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.480579e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 6.468661e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 8.411407e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 9.072e-05 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000117064 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 6.863790e-01 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 8.864e-05 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.00011611 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 6.925411e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OMP method
Time CPU total OMP Events: 16 Tracks: 5141 Hits: 31449 = 2.380133e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
To bad->good: Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad_Good method
Time CPU total Bad_Good Events: 17 Tracks: 5353 Hits: 32810 = 1.419067e-03 s

Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.625061e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.335680e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.308918e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 6.553590e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.577920e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.521111e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 6.534760e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 8.671284e-04 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 9.232e-05 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000119925 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 7.008510e-01 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000101216 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000129938 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 6.961660e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OMP method
Time CPU total OMP Events: 17 Tracks: 5353 Hits: 32810 = 2.357006e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
To bad->good: Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad_Good method
Time CPU total Bad_Good Events: 18 Tracks: 5750 Hits: 35226 = 1.784086e-03 s

Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 1.866817e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.393600e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.330376e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 6.488588e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.641920e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.580715e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.468542e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 9.200573e-04 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.0001032 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000265121 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 6.880739e-01 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000100864 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000256777 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 6.878071e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OMP method
Time CPU total OMP Events: 18 Tracks: 5750 Hits: 35226 = 2.197981e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
To bad->good: Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad_Good method
Time CPU total Bad_Good Events: 19 Tracks: 6260 Hits: 38329 = 1.860142e-03 s

Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 2.063036e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.389120e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.339912e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 6.514919e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.693440e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.649857e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 6.477110e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.021862e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000101984 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000259876 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 6.899428e-01 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000102176 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000262022 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 6.804729e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OMP method
Time CPU total OMP Events: 19 Tracks: 6260 Hits: 38329 = 2.449989e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
To bad->good: Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad_Good method
Time CPU total Bad_Good Events: 20 Tracks: 6560 Hits: 40004 = 1.965761e-03 s

Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 2.091885e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.418880e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.361370e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 6.560960e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.677440e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.618862e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 6.468530e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.042843e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000107328 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000273943 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 6.901610e-01 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000115392 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000277042 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 6.854939e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OMP method
Time CPU total OMP Events: 20 Tracks: 6560 Hits: 40004 = 2.690077e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
To bad->good: Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad_Good method
Time CPU total Bad_Good Events: 30 Tracks: 9310 Hits: 57158 = 2.976179e-03 s

Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.408909e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.792640e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.740456e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.493671e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.203840e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.140999e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 6.470821e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.497984e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.00014352 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.0001719 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 6.922462e-01 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000127328 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000164032 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 6.878922e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OMP method
Time CPU total OMP Events: 30 Tracks: 9310 Hits: 57158 = 3.426075e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
To bad->good: Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad_Good method
Time CPU total Bad_Good Events: 40 Tracks: 12174 Hits: 75953 = 3.677845e-03 s

Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 3.784180e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 1.993920e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 1.959801e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 6.523912e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.514560e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.470016e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 6.527891e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 1.982927e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.00016688 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000195026 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 6.913662e-01 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000157408 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000189066 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 6.876290e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OMP method
Time CPU total OMP Events: 40 Tracks: 12174 Hits: 75953 = 4.342079e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
To bad->good: Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad_Good method
Time CPU total Bad_Good Events: 50 Tracks: 15024 Hits: 93663 = 4.482031e-03 s

Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 5.064964e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.227520e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.210140e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 6.530070e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.766720e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.748966e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.483481e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.496004e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000207648 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000241041 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 8.029451e-01 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000169792 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.00019908 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 6.839590e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OMP method
Time CPU total OMP Events: 50 Tracks: 15024 Hits: 93663 = 5.374193e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
To bad->good: Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad_Good method
Time CPU total Bad_Good Events: 60 Tracks: 18356 Hits: 114400 = 5.780697e-03 s

Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 8.940935e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.562880e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.539158e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.577039e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.093120e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.089905e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 6.471720e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 2.876997e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000244928 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000431061 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 6.973341e-01 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000199968 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000354052 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 6.902809e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OMP method
Time CPU total OMP Events: 60 Tracks: 18356 Hits: 114400 = 5.882025e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
To bad->good: Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad_Good method
Time CPU total Bad_Good Events: 70 Tracks: 21691 Hits: 135212 = 6.351948e-03 s

Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 6.587029e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.731840e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.720356e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 6.542389e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.436160e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.440380e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.497891e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 3.527164e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000270816 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000432968 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 6.921439e-01 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000220992 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000375986 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 6.930499e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OMP method
Time CPU total OMP Events: 70 Tracks: 21691 Hits: 135212 = 7.014036e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
To bad->good: Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad_Good method
Time CPU total Bad_Good Events: 80 Tracks: 24967 Hits: 155915 = 7.406950e-03 s

Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 7.876158e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.098560e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.108978e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.528220e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.922880e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.931522e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.501830e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.060984e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000296736 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000454187 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 7.032120e-01 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000248576 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000411987 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 6.924679e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OMP method
Time CPU total OMP Events: 80 Tracks: 24967 Hits: 155915 = 7.387877e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
To bad->good: Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad_Good method
Time CPU total Bad_Good Events: 90 Tracks: 28121 Hits: 175352 = 8.372068e-03 s

Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 9.339094e-03 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.352640e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.359318e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 6.598148e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.274880e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.291534e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 6.608860e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 4.620790e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000345152 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.00048995 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 6.984270e-01 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000271616 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000443935 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 7.040870e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OMP method
Time CPU total OMP Events: 90 Tracks: 28121 Hits: 175352 = 7.894039e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
To bad->good: Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad_Good method
Time CPU total Bad_Good Events: 100 Tracks: 31506 Hits: 196714 = 9.525299e-03 s

Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 9.875059e-03 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.625920e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.681183e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.577580e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.681600e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.708767e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.526871e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 5.120039e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000380224 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000536203 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 6.906281e-01 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000299296 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000464916 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 6.941340e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OMP method
Time CPU total OMP Events: 100 Tracks: 31506 Hits: 196714 = 9.516954e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
To bad->good: Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad_Good method
Time CPU total Bad_Good Events: 110 Tracks: 34413 Hits: 214801 = 1.085210e-02 s

Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.224399e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.837120e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.888607e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.516812e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.015680e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.040169e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.537931e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 5.587816e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000390144 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000547886 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 7.013090e-01 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000319168 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000478029 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 6.913998e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OMP method
Time CPU total OMP Events: 110 Tracks: 34413 Hits: 214801 = 9.592056e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
To bad->good: Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad_Good method
Time CPU total Bad_Good Events: 120 Tracks: 37459 Hits: 233881 = 1.187181e-02 s

Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.366806e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.156480e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.200935e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.545370e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.420800e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.462170e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.531720e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 6.136894e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.00044752 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000607014 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 6.942360e-01 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000347872 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000505924 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 7.020881e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OMP method
Time CPU total OMP Events: 120 Tracks: 37459 Hits: 233881 = 1.108789e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
To bad->good: Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad_Good method
Time CPU total Bad_Good Events: 130 Tracks: 40754 Hits: 254225 = 1.076102e-02 s

Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.084304e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.352320e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.410744e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 6.534760e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.726080e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.781651e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 6.540480e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 6.453037e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000472128 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000622988 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 6.859269e-01 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000370304 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000531197 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 6.915898e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OMP method
Time CPU total OMP Events: 130 Tracks: 40754 Hits: 254225 = 1.225281e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
To bad->good: Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad_Good method
Time CPU total Bad_Good Events: 140 Tracks: 44705 Hits: 278857 = 1.252413e-02 s

Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.350403e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.801920e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.880428e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 6.676300e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.283520e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.370544e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.592481e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 7.258177e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.00052 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000683069 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 7.090199e-01 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.0004072 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000571966 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 7.038310e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OMP method
Time CPU total OMP Events: 140 Tracks: 44705 Hits: 278857 = 1.242709e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
To bad->good: Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad_Good method
Time CPU total Bad_Good Events: 150 Tracks: 48106 Hits: 299938 = 1.374006e-02 s

Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.321006e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.034240e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.109310e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 6.627290e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.688320e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.768703e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.603150e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 7.915974e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000532672 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.00068593 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 6.988080e-01 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000428224 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000586033 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 6.919689e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OMP method
Time CPU total OMP Events: 150 Tracks: 48106 Hits: 299938 = 1.361609e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
To bad->good: Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad_Good method
Time CPU total Bad_Good Events: 160 Tracks: 51627 Hits: 321407 = 1.499128e-02 s

Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.503706e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.395840e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.490780e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 6.648979e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.055681e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.150173e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.571209e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 8.507013e-03 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.0005896 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000741005 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 6.981370e-01 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000452672 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000609159 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 6.958821e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OMP method
Time CPU total OMP Events: 160 Tracks: 51627 Hits: 321407 = 1.482821e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
To bad->good: Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad_Good method
Time CPU total Bad_Good Events: 170 Tracks: 54445 Hits: 339287 = 1.563478e-02 s

Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.724100e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.558400e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.671978e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 6.626351e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.359040e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.469654e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.597281e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 8.872032e-03 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000630656 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.00077796 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 7.032762e-01 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000481728 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000633001 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 6.962700e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OMP method
Time CPU total OMP Events: 170 Tracks: 54445 Hits: 339287 = 1.568604e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
To bad->good: Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad_Good method
Time CPU total Bad_Good Events: 180 Tracks: 57335 Hits: 357414 = 1.806903e-02 s

Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 1.743102e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.850240e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.958080e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.759162e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.696640e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.808208e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.679630e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 9.399176e-03 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000659072 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000806093 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 7.087750e-01 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000497888 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000663996 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 7.087688e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OMP method
Time CPU total OMP Events: 180 Tracks: 57335 Hits: 357414 = 1.536822e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
To bad->good: Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad_Good method
Time CPU total Bad_Good Events: 190 Tracks: 60572 Hits: 377316 = 1.685667e-02 s

Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 1.885200e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.145280e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.270409e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.714721e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.029761e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.158684e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 6.613190e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 9.439945e-03 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.00067808 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.00082612 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 6.971929e-01 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000520768 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000662088 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 1.042552e+00 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OMP method
Time CPU total OMP Events: 190 Tracks: 60572 Hits: 377316 = 1.689601e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
To bad->good: Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad_Good method
Time CPU total Bad_Good Events: 200 Tracks: 63921 Hits: 398354 = 1.860189e-02 s

Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 1.986694e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.478720e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.620884e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.780970e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.569920e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.687973e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 6.568990e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.012993e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.0007184 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000865936 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 6.999211e-01 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000556128 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000706196 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 6.964381e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OMP method
Time CPU total OMP Events: 200 Tracks: 63921 Hits: 398354 = 2.425694e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
To bad->good: Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad_Good method
Time CPU total Bad_Good Events: 210 Tracks: 66802 Hits: 416406 = 2.201986e-02 s

Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.181411e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.721601e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.880760e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.615810e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 8.932801e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.090900e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 6.659672e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.088405e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000774976 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000922918 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 7.015450e-01 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000575392 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.00072217 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 6.991920e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OMP method
Time CPU total OMP Events: 210 Tracks: 66802 Hits: 416406 = 1.914001e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
To bad->good: Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad_Good method
Time CPU total Bad_Good Events: 220 Tracks: 69786 Hits: 435132 = 2.122498e-02 s

Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.262807e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 6.979840e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.128716e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 6.778011e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.259841e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.410381e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 6.732171e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.119399e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.00078384 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000937939 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 7.124591e-01 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000601888 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000756979 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 7.126181e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OMP method
Time CPU total OMP Events: 220 Tracks: 69786 Hits: 435132 = 1.102304e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
To bad->good: Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad_Good method
Time CPU total Bad_Good Events: 230 Tracks: 72547 Hits: 452586 = 2.168107e-02 s

Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.374315e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.188800e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.350445e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 6.676960e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.632960e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.779930e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 6.631610e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.128316e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000797536 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000946999 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 6.910470e-01 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.000628576 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.0008111 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 7.081411e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OMP method
Time CPU total OMP Events: 230 Tracks: 72547 Hits: 452586 = 1.095080e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
To bad->good: Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad_Good method
Time CPU total Bad_Good Events: 240 Tracks: 76144 Hits: 474872 = 2.287698e-02 s

Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 2.768397e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.543041e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.700920e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 6.607201e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.000416e-03 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.015186e-03 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 6.574581e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.229000e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000844064 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000985861 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 6.994371e-01 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.0006488 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.000808954 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 6.936400e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OMP method
Time CPU total OMP Events: 240 Tracks: 76144 Hits: 474872 = 1.102185e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
To bad->good: Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad_Good method
Time CPU total Bad_Good Events: 250 Tracks: 79172 Hits: 493810 = 2.449203e-02 s

Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 2.854395e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.707840e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.879734e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 6.666460e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.031040e-03 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.049042e-03 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 6.588280e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.268101e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.000855296 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00100899 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 7.125330e-01 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000669248 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000814915 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 7.117989e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OMP method
Time CPU total OMP Events: 250 Tracks: 79172 Hits: 493810 = 1.264095e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
To bad->good: Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad_Good method
Time CPU total Bad_Good Events: 260 Tracks: 82444 Hits: 514314 = 2.421308e-02 s

Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.304697e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.983040e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.189678e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 6.667631e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.069184e-03 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.089096e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 6.682351e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.359916e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.000908224 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00105691 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 7.037468e-01 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000701056 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000855207 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 7.116470e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OMP method
Time CPU total OMP Events: 260 Tracks: 82444 Hits: 514314 = 1.241493e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
To bad->good: Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad_Good method
Time CPU total Bad_Good Events: 270 Tracks: 85661 Hits: 534537 = 2.403069e-02 s

Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 2.372384e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.277760e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.490086e-04 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 6.706212e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.106240e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.127005e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 6.655118e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.387787e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.000973344 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00111794 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 6.948841e-01 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000721632 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000874043 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 6.985550e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OMP method
Time CPU total OMP Events: 270 Tracks: 85661 Hits: 534537 = 2.342010e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
To bad->good: Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad_Good method
Time CPU total Bad_Good Events: 280 Tracks: 89234 Hits: 557160 = 2.730203e-02 s

Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 2.501798e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.622081e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.831024e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 6.702700e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.153920e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.177073e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.619081e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.411295e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.000993568 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00117612 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 7.175841e-01 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00075168 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.000910997 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 7.079880e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OMP method
Time CPU total OMP Events: 280 Tracks: 89234 Hits: 557160 = 1.442194e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
To bad->good: Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad_Good method
Time CPU total Bad_Good Events: 290 Tracks: 92347 Hits: 576611 = 2.620912e-02 s

Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 2.848911e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.864640e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 9.081364e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 6.718130e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.189152e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.210928e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 6.759720e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.435518e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00104192 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00119615 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 7.048540e-01 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000770208 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000912189 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 6.968341e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OMP method
Time CPU total OMP Events: 290 Tracks: 92347 Hits: 576611 = 1.667094e-02 s

-----------------------------
Test for Bad_Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
To bad->good: Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad_Good method
Time CPU total Bad_Good Events: 300 Tracks: 95876 Hits: 598963 = 2.783513e-02 s

Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 2.711582e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.223680e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.469986e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 6.725090e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.230688e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.255989e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 6.641860e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.500297e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00105648 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00120807 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 7.036650e-01 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000799296 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000947952 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 7.097690e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OMP method
Time CPU total OMP Events: 300 Tracks: 95876 Hits: 598963 = 2.178597e-02 s

-----------------------------
