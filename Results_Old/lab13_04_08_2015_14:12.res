Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 2.229214e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.432000e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.987022e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 9.338403e-02 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.109120e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.060963e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.278110e-02 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 6.699562e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 5.4528e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 0.000200987 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 6.309762e-01 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.9648e-05 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 0.00022912 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 6.213238e-01 s

-----------------------------
Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 2.448559e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 8.774400e-05 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 8.296967e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 5.873489e-02 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.112000e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.058578e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 5.633116e-02 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 9.894371e-05 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 5.5168e-05 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 8.58307e-05 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 6.113720e-01 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 7.7856e-05 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 0.00010705 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 6.015780e-01 s

-----------------------------
Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 3.612041e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.057601e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.700920e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 5.747294e-02 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.135680e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.089573e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 5.775309e-02 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.509190e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 6.0928e-05 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 0.000211 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 5.966132e-01 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 8.2912e-05 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 0.000232935 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 6.009569e-01 s

-----------------------------
Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 6.589890e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.800000e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.296967e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 5.766582e-02 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.158720e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.111031e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 5.746198e-02 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.570152e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 6.3872e-05 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 0.0002141 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 6.003270e-01 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 8.4224e-05 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 0.000231981 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 5.983970e-01 s

-----------------------------
Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 9.269714e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.867200e-05 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.416176e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 5.422592e-02 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.114880e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.080036e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 5.182099e-02 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.750324e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 6.576e-05 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 9.70364e-05 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 6.127360e-01 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 8.4416e-05 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 0.000117064 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 5.994668e-01 s

-----------------------------
Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 9.758472e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 9.260800e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 9.012222e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 5.732679e-02 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.157760e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.130104e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 5.768800e-02 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 4.060268e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 6.7552e-05 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 9.98974e-05 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 5.970399e-01 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 8.7424e-05 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 0.000121117 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 5.950880e-01 s

-----------------------------
Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 1.137972e-03 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 1.030080e-04 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.799004e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 5.851197e-02 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.289280e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.251698e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 5.804205e-02 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 4.799366e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 7.5488e-05 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 0.000231981 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 5.935159e-01 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 9.408e-05 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 0.000247002 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.025321e-01 s

-----------------------------
Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 1.211882e-03 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.068480e-04 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.020432e-04 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.007791e-02 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.275200e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.220703e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 5.831599e-02 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 5.080700e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 7.712e-05 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 0.000226974 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 6.148908e-01 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 9.312e-05 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.000243902 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 5.939262e-01 s

-----------------------------
Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 9.779930e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.057920e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.008511e-04 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.017590e-02 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.297600e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.249313e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 5.652881e-02 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 5.381107e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 5.6096e-05 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 0.000375986 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.039271e-01 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 9.9744e-05 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 0.000254869 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 5.981171e-01 s

-----------------------------
Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 1.171112e-03 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.119360e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.080036e-04 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 5.780602e-02 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.389440e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.358986e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 5.792713e-02 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 6.029606e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 5.9872e-05 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 0.000433922 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.056352e-01 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 7.9584e-05 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 0.000439167 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 5.993938e-01 s

-----------------------------
Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.613855e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.148160e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.108646e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 5.767703e-02 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.317120e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.280308e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 5.713701e-02 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 6.971359e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 8.7744e-05 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000246048 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 6.013589e-01 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000106368 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000258923 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 6.014850e-01 s

-----------------------------
Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.653910e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.167680e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.130104e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 5.889416e-02 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.350400e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.311302e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 5.797386e-02 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 7.390976e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 9.0464e-05 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000243902 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 6.034191e-01 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000109952 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000262976 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 5.974970e-01 s

-----------------------------
Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 2.019167e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.242880e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.199245e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 5.747819e-02 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.420800e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.380444e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 5.760193e-02 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 8.540154e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 9.7728e-05 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000248909 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 6.056859e-01 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000112576 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000264883 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 5.986590e-01 s

-----------------------------
Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 2.040863e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.257600e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.208782e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 5.407405e-02 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.433600e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.389980e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 5.756998e-02 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 8.580685e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 9.968e-05 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000252008 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 5.961630e-01 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000113696 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000265837 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 5.947781e-01 s

-----------------------------
Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 2.228975e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.264000e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.239777e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 5.716920e-02 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.535040e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.490116e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 5.678082e-02 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 9.899139e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000103264 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000255108 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 5.949061e-01 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.0001224 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000275135 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 6.015511e-01 s

-----------------------------
Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 2.297163e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.380160e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.339912e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.275511e-02 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.565760e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.511574e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 5.968809e-02 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 9.911060e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000103008 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000252008 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 6.035190e-01 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.00012336 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000273943 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 6.045370e-01 s

-----------------------------
Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.844883e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.308160e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.258850e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 5.634809e-02 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.576640e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.528263e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 5.652499e-02 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 1.054049e-03 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000103552 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000255108 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 5.995779e-01 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000126048 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000277996 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 6.011670e-01 s

-----------------------------
Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 2.182007e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.425600e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.380444e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 5.493999e-02 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.632000e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.590252e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.104684e-02 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 1.113892e-03 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000112256 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000266075 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 6.007719e-01 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000131328 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000283957 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 5.937080e-01 s

-----------------------------
Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 2.385855e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.384000e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.339912e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 5.689001e-02 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.679040e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.628399e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 5.885315e-02 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.216888e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000112 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000142097 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 6.033461e-01 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000135072 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000168085 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 6.000600e-01 s

-----------------------------
Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 2.358198e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.461440e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.420975e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 5.940986e-02 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.675840e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.630783e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 5.881310e-02 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.263142e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000113536 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000271797 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 6.003928e-01 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000136064 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000289917 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 6.018879e-01 s

-----------------------------
Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.955126e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.511680e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.630783e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.057692e-02 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 1.907200e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.019405e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 6.229806e-02 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.825094e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000146624 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000302076 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 6.056461e-01 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.00017744 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000329971 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 6.095481e-01 s

-----------------------------
Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 4.410982e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 1.949120e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 1.897812e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 5.935192e-02 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.433600e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.381802e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 5.933213e-02 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 2.437115e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000170528 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000324011 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 6.031642e-01 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000207328 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000360012 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 6.003449e-01 s

-----------------------------
Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 5.793095e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.136640e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.110004e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 5.916190e-02 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.730560e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.670288e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.059885e-02 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.938032e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000190368 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.0003438 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 6.018991e-01 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000231968 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000385046 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 5.993950e-01 s

-----------------------------
Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 7.625103e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.208960e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.422333e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.209588e-02 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 2.865600e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 2.892017e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 5.886102e-02 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 3.648043e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000197344 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000537872 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 6.050920e-01 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000240896 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000623941 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 6.120522e-01 s

-----------------------------
Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 7.608175e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.425920e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.648830e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 6.603909e-02 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.218240e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.240108e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.053400e-02 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 4.299879e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000217664 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000599146 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 6.080499e-01 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000273248 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000591993 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 6.108201e-01 s

-----------------------------
Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 9.084940e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 2.661760e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 2.911091e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.239295e-02 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.617280e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.681183e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.077194e-02 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.920959e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000246336 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000642061 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 6.166430e-01 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.00031008 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.00068903 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 6.136820e-01 s

-----------------------------
Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 1.079392e-02 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 2.902080e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.170967e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 5.958605e-02 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 3.919360e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.000664e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 5.883718e-02 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 5.501986e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000272032 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000614166 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 6.012201e-01 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.00034672 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000726938 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 6.040881e-01 s

-----------------------------
Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 1.241612e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.357440e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.471375e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.260085e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.298560e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.398823e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.053686e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 6.280899e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000304288 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.00068903 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 6.017311e-01 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000381472 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000735044 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 6.065059e-01 s

-----------------------------
Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.419902e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.378560e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.681183e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.625390e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 4.403200e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 4.699230e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.137919e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 6.845951e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000322656 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000638962 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 6.340251e-01 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000413408 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.00079608 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 6.128769e-01 s

-----------------------------
Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.546597e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 3.774720e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.069805e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.486583e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 4.784320e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.099773e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.508517e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 7.394075e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000357536 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.00067091 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 6.235418e-01 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000451456 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.0007689 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 6.134810e-01 s

-----------------------------
Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.427102e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 3.949120e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.298687e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 6.462598e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.209920e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.381107e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 6.355906e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 8.048058e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000373408 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000731945 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 6.082718e-01 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000482816 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000888109 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 6.059251e-01 s

-----------------------------
Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.586986e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.433280e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.620552e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 6.636786e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 5.584960e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 5.950928e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.614494e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 8.768082e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000421888 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000737906 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 6.226070e-01 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000530304 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000910044 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 6.092951e-01 s

-----------------------------
Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.698184e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 4.551360e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 4.930496e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 7.036090e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 5.955840e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.341934e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.652904e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 9.524822e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000438016 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000898123 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 6.107731e-01 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000562208 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000900984 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 6.133950e-01 s

-----------------------------
Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.870203e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 4.847680e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.259514e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 6.979799e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.232320e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.630421e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.386805e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 1.006413e-02 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.00047792 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000800133 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 6.121860e-01 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000599104 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000921011 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 6.139009e-01 s

-----------------------------
Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.984715e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.084481e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.519390e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 7.738781e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.538240e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.949902e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.769300e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 1.064706e-02 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.00049744 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000876904 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 6.235111e-01 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000628928 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.0010221 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 6.169889e-01 s

-----------------------------
Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 2.139401e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.271360e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.719662e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 7.959795e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.855041e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.300377e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.676817e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 1.143789e-02 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000518656 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000868082 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 6.186430e-01 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000661152 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.00105715 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 6.062438e-01 s

-----------------------------
Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 2.371097e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 5.477760e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 5.919933e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 7.139111e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 7.151360e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 7.619858e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 6.443310e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 1.180291e-02 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000542048 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.00087285 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 6.140232e-01 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000688416 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.00108385 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 6.269171e-01 s

-----------------------------
Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 2.475405e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 5.799361e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.310940e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 7.627296e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 7.614720e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.108616e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 6.522083e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.249194e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000570272 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000993013 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 6.177671e-01 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000751968 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000935078 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 6.095529e-01 s

-----------------------------
Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.678990e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.077440e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.570816e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 7.467103e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 8.006720e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 8.521080e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 6.660008e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.324296e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000601856 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000931978 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 6.104510e-01 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000767936 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.00113702 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 6.125209e-01 s

-----------------------------
Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.780700e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 6.461440e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 6.978512e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.819200e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 8.271040e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 8.809566e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 6.794715e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.363087e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000635616 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.00103903 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 6.174510e-01 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000793568 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.00112796 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 6.225870e-01 s

-----------------------------
Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.924800e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 6.535361e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.081032e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 8.129597e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 8.620480e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.169579e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 7.131600e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.376605e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.0006456 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.00097394 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 6.228120e-01 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.000827936 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.00119996 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 6.138451e-01 s

-----------------------------
Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 3.176379e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 6.944960e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.519722e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.737303e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 8.939840e-04 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 9.510517e-04 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 6.803417e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.444006e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000671296 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.00111985 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 6.476660e-01 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00086064 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00124812 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 6.176581e-01 s

-----------------------------
Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 3.269005e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.089280e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.669926e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.294393e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 9.242240e-04 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 9.829998e-04 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 6.560707e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.568985e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.000704544 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00108409 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 6.294839e-01 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.00088928 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.00127482 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 6.217320e-01 s

-----------------------------
Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.912402e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.461761e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.870197e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.731485e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 9.608321e-04 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.246929e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 7.932401e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.568794e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.000739328 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00114489 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 6.465268e-01 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000927456 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.00131702 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 6.150889e-01 s

-----------------------------
Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 3.008699e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 7.640640e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 1.043081e-03 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.016419e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.002144e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.275063e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 6.861496e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.643395e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.000759392 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00114608 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 6.322980e-01 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000962368 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.00131893 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 6.215940e-01 s

-----------------------------
Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 3.195882e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 7.872640e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.530617e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 7.746887e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.040768e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.106024e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.785297e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.746106e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00078768 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00116587 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 6.473329e-01 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00100042 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00137401 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 6.108069e-01 s

-----------------------------
Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 3.284001e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.151361e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.811951e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 7.678413e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.072800e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.137018e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 7.000399e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.761794e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.000808896 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00112414 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 6.247139e-01 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.00103174 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.0014081 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 6.279290e-01 s

-----------------------------
Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 3.423095e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 8.500800e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.160042e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 8.244801e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.118208e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.185894e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 7.330585e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.831698e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.000844352 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00119591 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 6.374681e-01 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.00107789 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.0014329 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 6.264749e-01 s

-----------------------------
