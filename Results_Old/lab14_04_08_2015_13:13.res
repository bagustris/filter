Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.499653e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.102400e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.700920e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 6.822441e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.117760e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.060963e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.629622e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 5.507469e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 4.4896e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.10487e-05 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.360761e-01 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 5.312e-05 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 8.2016e-05 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.470210e-01 s

-----------------------------
Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 1.950264e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 1.024320e-04 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 9.799004e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 6.859901e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.356800e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.311302e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 6.880860e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 1.108646e-04 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 4.7808e-05 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.60555e-05 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 7.333691e-01 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 5.3504e-05 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 8.29697e-05 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 7.213361e-01 s

-----------------------------
Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 2.691746e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.441601e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.106232e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 6.872599e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.122880e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.089573e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 6.890931e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.261234e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 4.6368e-05 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 7.31945e-05 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 7.239411e-01 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 5.5968e-05 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 8.29697e-05 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 7.224751e-01 s

-----------------------------
Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 5.099773e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.288000e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.106232e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 6.868961e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.322880e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.280308e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 6.858752e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.100468e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 4.9024e-05 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 7.60555e-05 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 7.231028e-01 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 5.7856e-05 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 8.51154e-05 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 7.215610e-01 s

-----------------------------
Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 7.629395e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.499200e-05 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.201599e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 6.848960e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.183040e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.139641e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 6.851869e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.180504e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 5.5008e-05 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 8.10623e-05 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 7.272210e-01 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 6.0128e-05 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 9.08375e-05 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 7.226250e-01 s

-----------------------------
Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 7.679462e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 9.196800e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.797646e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 6.827519e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.190400e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.139641e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 6.871061e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 3.468990e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 5.8368e-05 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 9.39369e-05 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 7.064390e-01 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 6.0128e-05 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 8.89301e-05 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 7.164609e-01 s

-----------------------------
Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 9.059906e-04 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 1.010880e-04 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.703636e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 6.921809e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.291840e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.251698e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 6.897380e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 3.941059e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 7.2832e-05 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 9.98974e-05 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 7.207899e-01 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 6.5728e-05 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 9.60827e-05 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 7.189012e-01 s

-----------------------------
Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 9.441376e-04 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.571201e-05 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.298325e-05 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.846101e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.292160e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.251698e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 6.844552e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 4.107952e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 6.2176e-05 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 9.20296e-05 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 7.288449e-01 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 6.4832e-05 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 9.20296e-05 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 7.219660e-01 s

-----------------------------
Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 7.750988e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.042240e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 9.989738e-05 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.820180e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.288640e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.349449e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 6.840391e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 4.417896e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 6.2528e-05 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 8.89301e-05 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 7.112100e-01 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 6.496e-05 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 9.17912e-05 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 7.165380e-01 s

-----------------------------
Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 9.131432e-04 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.055680e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.018047e-04 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 6.880660e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.295680e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.261234e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 6.843541e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 4.937649e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 6.8384e-05 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 9.60827e-05 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 7.198730e-01 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 6.8416e-05 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 9.58443e-05 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 7.292500e-01 s

-----------------------------
Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.353025e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.131840e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.089573e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.900861e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.360000e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.330376e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.848052e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 5.779266e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.3792e-05 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 9.98974e-05 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.219970e-01 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.5072e-05 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000102043 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.247171e-01 s

-----------------------------
Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.276970e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.276480e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.239777e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 6.825290e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.388480e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.361370e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 6.873381e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 6.060600e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 7.568e-05 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000102997 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 7.282751e-01 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 7.6224e-05 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000103951 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 7.297449e-01 s

-----------------------------
Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 1.684904e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.244480e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.199245e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 6.874990e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.476800e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.440048e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 6.894739e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 7.059574e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 8.672e-05 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.00011611 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 7.163839e-01 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 8.24e-05 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000108957 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 7.252240e-01 s

-----------------------------
Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 1.728058e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.302720e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.270771e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 6.858900e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.456000e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.418591e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 6.882689e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 7.419586e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 8.7968e-05 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000118017 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 7.246251e-01 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 8.2976e-05 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000111103 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 7.204490e-01 s

-----------------------------
Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 1.787901e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.362880e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.318455e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 6.892290e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.564800e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.528263e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 6.852961e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 8.189678e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 9.1008e-05 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000118971 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 7.209392e-01 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 9.3472e-05 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000122786 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 7.230241e-01 s

-----------------------------
Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 1.840830e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.361280e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.320839e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.618500e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.583360e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.549721e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 6.658070e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 8.320808e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 9.3088e-05 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000120878 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 7.134271e-01 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 9.0592e-05 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000118971 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 7.192800e-01 s

-----------------------------
Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.564026e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.384320e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.339912e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 6.961710e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.725440e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.680851e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 6.913421e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 8.711815e-04 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 9.8208e-05 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.00012517 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 7.300611e-01 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 9.2992e-05 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000120878 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 7.263970e-01 s

-----------------------------
Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 1.725197e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.405760e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.370907e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 6.900449e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.699840e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.637936e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.872110e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 9.160042e-04 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000102624 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000261068 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 7.232220e-01 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 9.9744e-05 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000257015 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 7.225001e-01 s

-----------------------------
Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 2.026081e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.429440e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.380444e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 6.852100e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.692480e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.659393e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 6.879861e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.006842e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.00010272 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000260115 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 7.272191e-01 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000102816 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000261068 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 7.250760e-01 s

-----------------------------
Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 2.111912e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.466560e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.420975e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 6.887281e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.772160e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.718998e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 6.889250e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.048088e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000109856 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000267982 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 7.298939e-01 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000104096 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000258923 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 7.270799e-01 s

-----------------------------
Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.311157e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.788480e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.740456e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.916959e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.212480e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.150536e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 1.004793e+00 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.506090e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.00013616 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000164032 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 7.232389e-01 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000129536 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000159979 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 7.224300e-01 s

-----------------------------
Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 3.741980e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.084480e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.050400e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 6.840711e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.542400e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.501011e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 6.890221e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 2.014875e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000173472 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000200987 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 7.247789e-01 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000157504 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000189066 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 7.254360e-01 s

-----------------------------
Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 4.983902e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.243200e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.219677e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 6.918960e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.807360e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.779961e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.867161e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.462149e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.00019104 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000218868 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 7.269599e-01 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.00017296 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000203848 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 7.235129e-01 s

-----------------------------
Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 6.644964e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.516480e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.510548e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.931140e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.147200e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.139973e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 6.843412e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 3.057003e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000238784 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000400066 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 7.303030e-01 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000201504 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000354052 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 7.212050e-01 s

-----------------------------
Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 6.496906e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.740160e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.748966e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 6.926000e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.456320e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.459454e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.874928e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 3.556013e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000260128 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000414133 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 7.247381e-01 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000220352 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000372887 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 7.245190e-01 s

-----------------------------
Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 7.739067e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.045120e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.051758e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.954050e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.936320e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.950596e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.812680e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.163027e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000304416 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000459909 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 7.282400e-01 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000249248 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000405073 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 7.341011e-01 s

-----------------------------
Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 9.114027e-03 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.316480e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.330708e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 6.923192e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.296320e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.308224e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 6.961951e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 4.606962e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000330688 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.00048399 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 7.387421e-01 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000270368 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000426054 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 7.405410e-01 s

-----------------------------
Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 1.069403e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.687040e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.700256e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.962450e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.697920e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.730225e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.993680e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 5.236149e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000378688 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000534058 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 7.415020e-01 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000297056 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000452995 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 7.289939e-01 s

-----------------------------
Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.199007e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.912960e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.948212e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.995790e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.048321e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.080700e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.902030e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 5.614996e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.00039632 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000551939 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 7.235701e-01 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000321728 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000477076 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 7.282801e-01 s

-----------------------------
Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.336193e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.176960e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.229546e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.930201e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.462400e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.509853e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.912889e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 6.212950e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.00043632 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000591993 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 7.384350e-01 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000349568 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000504971 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 7.327120e-01 s

-----------------------------
Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.179695e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.454400e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.510880e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 6.958058e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.786560e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.850792e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 6.858900e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 6.748915e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.00046256 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000620842 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 7.332129e-01 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000372 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000527859 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 7.315710e-01 s

-----------------------------
Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.265192e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.845760e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.920959e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 6.966801e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.381760e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.449223e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.975510e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 7.354975e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.00050928 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000666142 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 7.346461e-01 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000404192 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000560045 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 7.369699e-01 s

-----------------------------
Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.458192e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.129281e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.199909e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 7.002060e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.699840e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.778240e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.966679e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 7.822037e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000542944 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000702858 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 7.406621e-01 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.0004304 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.00058794 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 7.403820e-01 s

-----------------------------
Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.580215e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.375040e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.459785e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 6.987140e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.133120e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.231236e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.976779e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 8.131027e-03 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000575488 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000724792 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 7.461171e-01 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000455296 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000607014 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 7.342951e-01 s

-----------------------------
Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.686192e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.630720e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.731583e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 6.938140e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.449600e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.560253e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.975532e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 8.922100e-03 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000618208 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000766993 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 7.356770e-01 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000479392 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000632048 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 7.338221e-01 s

-----------------------------
Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 1.824212e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.918081e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.039143e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 7.005670e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.791360e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.879734e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.942620e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 9.521008e-03 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.00066928 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000818014 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 7.280059e-01 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000500864 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000658035 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 7.366860e-01 s

-----------------------------
Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 1.977015e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.175040e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.308556e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.939390e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.064641e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.180141e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 7.031629e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 9.838104e-03 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000658976 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000808954 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 7.311780e-01 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000522272 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.000674009 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 7.392640e-01 s

-----------------------------
Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 2.127814e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.482881e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.618500e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.978021e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.565761e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.721352e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 6.992719e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.050496e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000741408 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000892162 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 7.411699e-01 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000554112 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000706911 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 7.420430e-01 s

-----------------------------
Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.254081e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.749440e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.890297e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 7.002411e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.088321e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.241104e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 7.000370e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.013088e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000765184 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000915051 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 7.415600e-01 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000581312 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.00073719 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 7.245948e-01 s

-----------------------------
Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.402902e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.045440e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.200241e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.004089e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.300160e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.448528e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 6.950018e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.105404e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000781856 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000931978 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 7.292469e-01 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.00060352 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000756979 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 7.370901e-01 s

-----------------------------
Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.521515e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.204160e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.379055e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.012141e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.713920e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.849072e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 6.594732e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.136899e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000815744 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.000967979 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 7.432179e-01 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.000628192 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.000781059 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 7.334621e-01 s

-----------------------------
Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 2.673602e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.488000e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.641315e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.051961e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.002048e-03 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.019955e-03 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 6.968820e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.206589e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000858752 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.00101089 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 7.353141e-01 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.000652544 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.000804186 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 7.487712e-01 s

-----------------------------
Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 2.784991e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.761921e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.948875e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.021160e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.036704e-03 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.055002e-03 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 7.022779e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.255608e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.000892576 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00104403 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 7.582459e-01 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000669088 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.000823975 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 7.370410e-01 s

-----------------------------
Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.423716e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.104641e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.311272e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.020671e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.071808e-03 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.091003e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 6.985261e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.282001e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.000926784 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00107789 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 7.363541e-01 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000697856 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000858068 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 6.979189e-01 s

-----------------------------
Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 2.528095e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.307521e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.521080e-04 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 7.026999e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.116288e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.137018e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 6.966970e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.361918e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.000917568 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00107312 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 7.411749e-01 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000721888 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.000876904 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 7.331159e-01 s

-----------------------------
Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 2.656984e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.640960e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.890629e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 7.088830e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.157824e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.179934e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.905510e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.427102e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.000976672 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00113201 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 7.399149e-01 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.000750272 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.000902176 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 7.454059e-01 s

-----------------------------
Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 2.774405e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.928640e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 9.160042e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 7.109380e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.195456e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.218796e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 7.089481e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.468396e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00100406 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.001158 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 7.509971e-01 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000770656 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.000922918 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 7.000031e-01 s

-----------------------------
Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 2.874780e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.167680e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.398460e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 7.078140e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.242848e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.266956e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 7.064462e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.537704e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00106134 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00122094 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 7.518051e-01 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000800512 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.000948906 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 7.387319e-01 s

-----------------------------
