Test for Bad_Good
To import-> Hits: 2093, Tracks: 358 Events: 1
To bad->good: Hits: 2093, Tracks: 358 Events: 1
Filter with Bad_Good method
Time CPU total Bad_Good Events: 1 Tracks: 358 Hits: 2093 = 1.099110e-04 s

Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.549721e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.131201e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.891655e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.056220e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.101120e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.070499e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.968141e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 6.008148e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 0.000114097 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 0.000166178 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 1.084212e+00 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 0.000186377 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 0.000237942 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 1.025044e+00 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OMP method
Time CPU total OMP Events: 1 Tracks: 358 Hits: 2093 = 7.669926e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 3158, Tracks: 532 Events: 2
To bad->good: Hits: 3158, Tracks: 532 Events: 2
Filter with Bad_Good method
Time CPU total Bad_Good Events: 2 Tracks: 532 Hits: 3158 = 1.599789e-04 s

Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 2.090931e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 8.313600e-05 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 7.891655e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 6.885090e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.107840e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.068115e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 6.814132e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 8.296967e-05 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 9.6768e-05 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 0.000138998 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 1.088312e+00 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 0.00017745 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 0.000226974 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 1.065333e+00 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OMP method
Time CPU total OMP Events: 2 Tracks: 532 Hits: 3158 = 1.595020e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 4741, Tracks: 779 Events: 3
To bad->good: Hits: 4741, Tracks: 779 Events: 3
Filter with Bad_Good method
Time CPU total Bad_Good Events: 3 Tracks: 779 Hits: 4741 = 2.479553e-04 s

Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 2.758503e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.096000e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.700920e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 6.863821e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.133760e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.099110e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 6.551411e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.258850e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 0.000129363 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 0.000179052 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 1.090889e+00 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 0.000100109 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 0.000149012 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 1.013775e+00 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OMP method
Time CPU total OMP Events: 3 Tracks: 779 Hits: 4741 = 7.760525e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
To bad->good: Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad_Good method
Time CPU total Bad_Good Events: 4 Tracks: 1300 Hits: 8061 = 4.119873e-04 s

Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 5.931854e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.915200e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.511543e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 6.864960e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.158080e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.120567e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 6.887500e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.160072e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 0.000170231 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 0.000226021 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 1.125227e+00 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 0.000180666 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 0.000228882 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 1.056262e+00 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OMP method
Time CPU total OMP Events: 4 Tracks: 1300 Hits: 8061 = 8.130074e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
To bad->good: Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad_Good method
Time CPU total Bad_Good Events: 5 Tracks: 1914 Hits: 11850 = 5.872250e-04 s

Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 7.989407e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.953600e-05 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.583069e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 6.799061e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.175040e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.142025e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 6.876540e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.139973e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 0.000197466 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 0.000250101 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 1.074306e+00 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 0.000143352 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 0.000190973 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 1.037128e+00 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OMP method
Time CPU total OMP Events: 5 Tracks: 1914 Hits: 11850 = 8.339882e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
To bad->good: Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad_Good method
Time CPU total Bad_Good Events: 6 Tracks: 2089 Hits: 12849 = 6.771088e-04 s

Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 8.602142e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.912000e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.606911e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 6.970730e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.179200e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.139641e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 6.887639e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 3.337860e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 0.000183917 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 0.000232935 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 1.074401e+00 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 0.000140597 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 0.000190973 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 1.034031e+00 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OMP method
Time CPU total OMP Events: 6 Tracks: 2089 Hits: 12849 = 8.060932e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
To bad->good: Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad_Good method
Time CPU total Bad_Good Events: 7 Tracks: 2418 Hits: 14812 = 7.729530e-04 s

Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 9.899139e-04 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 1.005120e-04 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.608269e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 6.885221e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.274560e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.239777e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 6.856360e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 3.890991e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 0.000196335 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 0.000244141 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 1.104761e+00 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 0.000206935 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 0.000262976 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 1.041083e+00 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OMP method
Time CPU total OMP Events: 7 Tracks: 2418 Hits: 14812 = 8.268356e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
To bad->good: Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad_Good method
Time CPU total Bad_Good Events: 8 Tracks: 2586 Hits: 15814 = 8.411407e-04 s

Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 1.013994e-03 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.001920e-04 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 9.584427e-05 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.889460e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.287040e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.239777e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 6.894321e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 4.210472e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 0.000256391 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 0.000313044 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 1.053764e+00 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.000238354 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.000283957 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 1.009776e+00 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OMP method
Time CPU total OMP Events: 8 Tracks: 2586 Hits: 15814 = 1.259089e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
To bad->good: Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad_Good method
Time CPU total Bad_Good Events: 9 Tracks: 2764 Hits: 16842 = 8.289814e-04 s

Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 8.139610e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.034560e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 9.894371e-05 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.861670e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.296640e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.261234e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 6.887779e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 4.391670e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 0.000185229 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 0.000306129 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 1.056888e+00 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 0.00025791 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 0.000311136 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 1.094128e+00 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OMP method
Time CPU total OMP Events: 9 Tracks: 2764 Hits: 16842 = 8.151531e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
To bad->good: Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad_Good method
Time CPU total Bad_Good Events: 10 Tracks: 3094 Hits: 18940 = 9.200573e-04 s

Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 9.679794e-04 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.037120e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.001358e-04 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 6.922488e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.296640e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.261234e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 6.859410e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 4.961491e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 0.000331303 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 0.000416994 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 1.039900e+00 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 0.000193259 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 0.000241995 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 1.043142e+00 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OMP method
Time CPU total OMP Events: 10 Tracks: 3094 Hits: 18940 = 8.358955e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
To bad->good: Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad_Good method
Time CPU total Bad_Good Events: 11 Tracks: 3613 Hits: 22010 = 1.075983e-03 s

Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.326084e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.126720e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.080036e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.910470e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.372480e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.339912e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.913052e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 5.829334e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000221279 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000272989 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 1.103734e+00 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000196715 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000248909 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 1.069773e+00 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OMP method
Time CPU total OMP Events: 11 Tracks: 3613 Hits: 22010 = 8.280277e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
To bad->good: Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad_Good method
Time CPU total Bad_Good Events: 12 Tracks: 3745 Hits: 22930 = 1.147032e-03 s

Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.382113e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.160000e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.130104e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 6.913779e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.383040e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.339912e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 6.869361e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 6.098747e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000212719 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000288963 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 1.067963e+00 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000241449 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.00029397 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 9.851630e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OMP method
Time CPU total OMP Events: 12 Tracks: 3745 Hits: 22930 = 8.389950e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
To bad->good: Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad_Good method
Time CPU total Bad_Good Events: 13 Tracks: 4414 Hits: 27034 = 1.415014e-03 s

Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 1.753092e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.255360e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.211166e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 6.916621e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.467840e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.430511e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 6.908481e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 7.200241e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000192805 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000242949 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 1.127958e+00 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000141499 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000193119 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 1.091439e+00 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OMP method
Time CPU total OMP Events: 13 Tracks: 4414 Hits: 27034 = 8.208752e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
To bad->good: Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad_Good method
Time CPU total Bad_Good Events: 14 Tracks: 4587 Hits: 28064 = 1.479864e-03 s

Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 1.456976e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.272960e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.227856e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 6.604550e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.435840e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.389980e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 6.504431e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 7.629395e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000174442 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000228882 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 1.076142e+00 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000182744 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000233889 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 9.969270e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OMP method
Time CPU total OMP Events: 14 Tracks: 4587 Hits: 28064 = 8.239746e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
To bad->good: Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad_Good method
Time CPU total Bad_Good Events: 15 Tracks: 5044 Hits: 30895 = 1.646996e-03 s

Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 1.682997e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.394560e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.320839e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 6.720529e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.506560e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.468658e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 6.749351e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 8.211136e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000234221 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000283957 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 1.021674e+00 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000178941 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000231981 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 1.013360e+00 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OMP method
Time CPU total OMP Events: 15 Tracks: 5044 Hits: 30895 = 8.039474e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
To bad->good: Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad_Good method
Time CPU total Bad_Good Events: 16 Tracks: 5141 Hits: 31449 = 1.646996e-03 s

Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 1.805067e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.452480e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.401901e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.970282e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.516160e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.478195e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 6.556549e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 8.420944e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000249197 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000298023 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 1.039285e+00 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000201369 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000253916 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 9.797289e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OMP method
Time CPU total OMP Events: 16 Tracks: 5141 Hits: 31449 = 8.370876e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
To bad->good: Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad_Good method
Time CPU total Bad_Good Events: 17 Tracks: 5353 Hits: 32810 = 1.374960e-03 s

Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.664877e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.442880e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.389980e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 6.544940e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.616960e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.568794e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 6.553130e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 8.299351e-04 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000179791 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000227928 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 1.032812e+00 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000212361 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000267029 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 1.070465e+00 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OMP method
Time CPU total OMP Events: 17 Tracks: 5353 Hits: 32810 = 8.230209e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
To bad->good: Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad_Good method
Time CPU total Bad_Good Events: 18 Tracks: 5750 Hits: 35226 = 1.672983e-03 s

Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 1.667976e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.365440e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.311302e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 6.602082e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.623040e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.568794e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.496480e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 9.291172e-04 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000184557 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000240088 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 1.153309e+00 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.0002315 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000279903 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 9.660430e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OMP method
Time CPU total OMP Events: 18 Tracks: 5750 Hits: 35226 = 8.161068e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
To bad->good: Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad_Good method
Time CPU total Bad_Good Events: 19 Tracks: 6260 Hits: 38329 = 1.762629e-03 s

Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 1.897097e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.419200e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.380444e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 6.529121e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.709120e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.668930e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 6.509550e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.020908e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000255117 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000306845 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 1.110435e+00 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000238213 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000287056 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 1.011552e+00 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OMP method
Time CPU total OMP Events: 19 Tracks: 6260 Hits: 38329 = 8.430481e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
To bad->good: Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad_Good method
Time CPU total Bad_Good Events: 20 Tracks: 6560 Hits: 40004 = 1.931190e-03 s

Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 1.805067e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.384960e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.351833e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 6.847730e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.711680e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.659393e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 6.838958e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.058102e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000176706 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000231981 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 1.022558e+00 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000216161 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000264168 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 1.011410e+00 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OMP method
Time CPU total OMP Events: 20 Tracks: 6560 Hits: 40004 = 8.659363e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
To bad->good: Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad_Good method
Time CPU total Bad_Good Events: 30 Tracks: 9310 Hits: 57158 = 2.969265e-03 s

Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.345013e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.827840e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.771450e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.962299e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.182400e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.131462e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 6.891799e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.497030e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000297068 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000337839 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 1.125849e+00 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000253833 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000296116 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 9.924369e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OMP method
Time CPU total OMP Events: 30 Tracks: 9310 Hits: 57158 = 8.831024e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
To bad->good: Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad_Good method
Time CPU total Bad_Good Events: 40 Tracks: 12174 Hits: 75953 = 3.700972e-03 s

Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 3.731966e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.071360e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.028942e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 7.053840e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.554240e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.510548e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 6.899068e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 1.987934e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000266039 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000340223 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 1.068346e+00 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.00028361 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000340939 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 9.888830e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OMP method
Time CPU total OMP Events: 40 Tracks: 12174 Hits: 75953 = 9.109974e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
To bad->good: Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad_Good method
Time CPU total Bad_Good Events: 50 Tracks: 15024 Hits: 93663 = 4.534960e-03 s

Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 4.765034e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.258880e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.238750e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 6.915479e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.816960e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.789497e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.919160e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.501965e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000282833 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000452995 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 1.102764e+00 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000266829 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000437021 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 1.023225e+00 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OMP method
Time CPU total OMP Events: 50 Tracks: 15024 Hits: 93663 = 9.119511e-04 s

-----------------------------
Test for Bad_Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
To bad->good: Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad_Good method
Time CPU total Bad_Good Events: 60 Tracks: 18356 Hits: 114400 = 5.925894e-03 s

Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 6.644011e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.513920e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.501011e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.982710e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.166080e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.149509e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 6.887882e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 3.015041e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000317919 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000397205 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 1.156913e+00 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000300138 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000524044 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 1.052593e+00 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OMP method
Time CPU total OMP Events: 60 Tracks: 18356 Hits: 114400 = 1.093864e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
To bad->good: Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad_Good method
Time CPU total Bad_Good Events: 70 Tracks: 21691 Hits: 135212 = 6.482840e-03 s

Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 6.510973e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.753280e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.760887e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 7.088232e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.440640e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.430843e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.724999e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 3.545046e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000367259 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000417948 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 1.060777e+00 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000368988 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000413179 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 1.030991e+00 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OMP method
Time CPU total OMP Events: 70 Tracks: 21691 Hits: 135212 = 1.057148e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
To bad->good: Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad_Good method
Time CPU total Bad_Good Events: 80 Tracks: 24967 Hits: 155915 = 7.405996e-03 s

Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 7.614136e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.136000e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.149509e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.820030e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.894720e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.900528e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.978099e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.186869e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000394072 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000444174 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 1.080319e+00 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000268274 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000453949 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 1.010562e+00 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OMP method
Time CPU total OMP Events: 80 Tracks: 24967 Hits: 155915 = 1.106977e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
To bad->good: Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad_Good method
Time CPU total Bad_Good Events: 90 Tracks: 28121 Hits: 175352 = 8.389950e-03 s

Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 9.216070e-03 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.312000e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.340244e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 7.017362e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.279040e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.301071e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 6.974590e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 4.732132e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000408567 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000494957 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 1.090017e+00 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000362942 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.00041604 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 1.009458e+00 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OMP method
Time CPU total OMP Events: 90 Tracks: 28121 Hits: 175352 = 1.976967e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
To bad->good: Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad_Good method
Time CPU total Bad_Good Events: 100 Tracks: 31506 Hits: 196714 = 9.735107e-03 s

Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 1.074505e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.595200e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.631115e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.980789e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.754560e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.751682e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.909361e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 5.124092e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000374814 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000564098 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 1.053230e+00 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000464638 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000668049 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 1.019221e+00 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OMP method
Time CPU total OMP Events: 100 Tracks: 31506 Hits: 196714 = 1.192093e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
To bad->good: Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad_Good method
Time CPU total Bad_Good Events: 110 Tracks: 34413 Hits: 214801 = 1.045895e-02 s

Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.201010e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.934400e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.969669e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.962619e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.053760e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.090237e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.881251e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 5.625963e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000446771 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000488997 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 1.093934e+00 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000399412 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.00045085 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 1.023252e+00 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OMP method
Time CPU total OMP Events: 110 Tracks: 34413 Hits: 214801 = 1.262188e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
To bad->good: Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad_Good method
Time CPU total Bad_Good Events: 120 Tracks: 37459 Hits: 233881 = 1.173019e-02 s

Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.329613e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.168640e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.189014e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.993079e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.472320e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.521774e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.898739e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 6.117821e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000426304 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000602961 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 1.101305e+00 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000542927 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000616074 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 1.007330e+00 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OMP method
Time CPU total OMP Events: 120 Tracks: 37459 Hits: 233881 = 1.976013e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
To bad->good: Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad_Good method
Time CPU total Bad_Good Events: 130 Tracks: 40754 Hits: 254225 = 1.077986e-02 s

Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.180100e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.425600e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.489422e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 7.168391e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.822720e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.879402e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 7.014029e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 6.635904e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000443678 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000496864 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 1.140071e+00 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000387811 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000473022 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 1.029033e+00 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OMP method
Time CPU total OMP Events: 130 Tracks: 40754 Hits: 254225 = 2.189875e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
To bad->good: Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad_Good method
Time CPU total Bad_Good Events: 140 Tracks: 44705 Hits: 278857 = 1.336908e-02 s

Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.316500e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.966400e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 5.021095e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 6.969581e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.384000e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.461143e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.944051e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 7.426977e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000507487 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000683069 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 1.060974e+00 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000493885 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000545025 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 1.063794e+00 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OMP method
Time CPU total OMP Events: 140 Tracks: 44705 Hits: 278857 = 1.331091e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
To bad->good: Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad_Good method
Time CPU total Bad_Good Events: 150 Tracks: 48106 Hits: 299938 = 1.395202e-02 s

Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.432109e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.053121e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.137920e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 7.059631e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.682880e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.771088e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 7.042329e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 7.937193e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000526079 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.000612974 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 1.120859e+00 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000653162 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000704765 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 1.073336e+00 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OMP method
Time CPU total OMP Events: 150 Tracks: 48106 Hits: 299938 = 1.281023e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
To bad->good: Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad_Good method
Time CPU total Bad_Good Events: 160 Tracks: 51627 Hits: 321407 = 1.497698e-02 s

Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.575279e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.435840e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.519390e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 7.018321e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.107841e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.200241e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.042601e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 8.464098e-03 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000584374 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000771999 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 1.087593e+00 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000521297 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.000596046 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 1.027357e+00 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OMP method
Time CPU total OMP Events: 160 Tracks: 51627 Hits: 321407 = 1.436949e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
To bad->good: Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad_Good method
Time CPU total Bad_Good Events: 170 Tracks: 54445 Hits: 339287 = 1.571774e-02 s

Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.682186e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.636800e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.719662e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 7.108769e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.432961e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.529259e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.057850e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 8.955956e-03 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000670949 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000842094 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 1.125731e+00 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000661531 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000715017 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 1.008127e+00 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OMP method
Time CPU total OMP Events: 170 Tracks: 54445 Hits: 339287 = 1.338959e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
To bad->good: Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad_Good method
Time CPU total Bad_Good Events: 180 Tracks: 57335 Hits: 357414 = 1.697111e-02 s

Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 1.730204e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.934080e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.029606e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 7.075200e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.798400e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.910728e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.986790e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 9.406090e-03 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000616967 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000789881 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 1.076108e+00 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000665037 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000721931 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 1.028024e+00 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OMP method
Time CPU total OMP Events: 180 Tracks: 57335 Hits: 357414 = 1.397133e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
To bad->good: Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad_Good method
Time CPU total Bad_Good Events: 190 Tracks: 60572 Hits: 377316 = 1.739717e-02 s

Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 1.965022e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.143361e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.270409e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 7.034481e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.090560e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.220673e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 7.011600e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 1.014686e-02 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.00249982 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.00257301 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 1.126210e+00 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.00274289 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.00282001 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 1.010788e+00 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OMP method
Time CPU total OMP Events: 190 Tracks: 60572 Hits: 377316 = 1.407146e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
To bad->good: Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad_Good method
Time CPU total Bad_Good Events: 200 Tracks: 63921 Hits: 398354 = 1.858377e-02 s

Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 2.103710e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.424320e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.561279e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 7.094171e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.595840e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.730888e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 7.057719e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.040411e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.0029139 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.00308895 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 1.064924e+00 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.00173692 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.00179291 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 1.039935e+00 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OMP method
Time CPU total OMP Events: 200 Tracks: 63921 Hits: 398354 = 1.444817e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
To bad->good: Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad_Good method
Time CPU total Bad_Good Events: 210 Tracks: 66802 Hits: 416406 = 2.182508e-02 s

Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.224708e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.771520e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.909370e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 7.203739e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.033601e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.181499e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 7.064369e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.098108e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.00078749 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000943899 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 1.071656e+00 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.00073641 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000844002 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 1.037246e+00 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OMP method
Time CPU total OMP Events: 210 Tracks: 66802 Hits: 416406 = 1.479149e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
To bad->good: Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad_Good method
Time CPU total Bad_Good Events: 220 Tracks: 69786 Hits: 435132 = 2.094579e-02 s

Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.358007e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.095361e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.228851e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.054121e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.288641e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.441376e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 7.076802e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.104116e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.00092719 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.00100803 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 1.119343e+00 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000697383 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000839949 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 1.061377e+00 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OMP method
Time CPU total OMP Events: 220 Tracks: 69786 Hits: 435132 = 2.474070e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
To bad->good: Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad_Good method
Time CPU total Bad_Good Events: 230 Tracks: 72547 Hits: 452586 = 2.210402e-02 s

Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.483892e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.258240e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.429123e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.122772e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.679360e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.829998e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 7.034180e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.197910e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.00330817 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.00340509 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 1.067372e+00 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.00154501 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.0016439 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 1.040194e+00 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OMP method
Time CPU total OMP Events: 230 Tracks: 72547 Hits: 452586 = 2.471924e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
To bad->good: Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad_Good method
Time CPU total Bad_Good Events: 240 Tracks: 76144 Hits: 474872 = 2.305222e-02 s

Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 2.657413e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.537920e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.719994e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.154150e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.004416e-03 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.022100e-03 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 7.069790e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.262093e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.00204195 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.00211978 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 1.084648e+00 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00195077 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00201106 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 1.033426e+00 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OMP method
Time CPU total OMP Events: 240 Tracks: 76144 Hits: 474872 = 1.372099e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
To bad->good: Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad_Good method
Time CPU total Bad_Good Events: 250 Tracks: 79172 Hits: 493810 = 2.469301e-02 s

Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 2.775407e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.784000e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.967949e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.181461e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.037312e-03 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.055956e-03 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 6.947858e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.232290e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00230636 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00237393 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 1.075316e+00 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.0010836 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.00114298 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 1.077729e+00 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OMP method
Time CPU total OMP Events: 250 Tracks: 79172 Hits: 493810 = 3.630877e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
To bad->good: Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad_Good method
Time CPU total Bad_Good Events: 260 Tracks: 82444 Hits: 514314 = 2.339983e-02 s

Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.429104e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.050241e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.251667e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.092550e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.075424e-03 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.094103e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 7.017951e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.320505e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00187793 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.00195098 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 1.092201e+00 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000918455 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.000967026 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 1.021536e+00 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OMP method
Time CPU total OMP Events: 260 Tracks: 82444 Hits: 514314 = 2.344847e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
To bad->good: Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad_Good method
Time CPU total Bad_Good Events: 270 Tracks: 85661 Hits: 534537 = 2.436709e-02 s

Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 2.520394e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.326721e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.530617e-04 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 7.176559e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.116608e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.135826e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 7.038329e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.378512e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00129202 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00135303 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 1.099090e+00 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.00113385 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.00121498 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 1.039375e+00 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OMP method
Time CPU total OMP Events: 270 Tracks: 85661 Hits: 534537 = 2.435923e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
To bad->good: Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad_Good method
Time CPU total Bad_Good Events: 280 Tracks: 89234 Hits: 557160 = 2.587891e-02 s

Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 2.653790e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.657920e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.878708e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 7.153540e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.163776e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.186132e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.973729e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.419210e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00195075 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00201702 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 1.100981e+00 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00228797 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00260305 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 1.040595e+00 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OMP method
Time CPU total OMP Events: 280 Tracks: 89234 Hits: 557160 = 2.766848e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
To bad->good: Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad_Good method
Time CPU total Bad_Good Events: 290 Tracks: 92347 Hits: 576611 = 2.682781e-02 s

Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 2.759004e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.963200e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 9.188652e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 7.035329e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.191264e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.214981e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 6.990280e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.518106e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00110683 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00115204 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 1.091317e+00 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.00234047 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.00239801 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 1.085968e+00 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OMP method
Time CPU total OMP Events: 290 Tracks: 92347 Hits: 576611 = 1.991987e-03 s

-----------------------------
Test for Bad_Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
To bad->good: Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad_Good method
Time CPU total Bad_Good Events: 300 Tracks: 95876 Hits: 598963 = 2.811813e-02 s

Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 2.888584e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.219841e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.450912e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 7.268591e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.237248e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.260996e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 6.776569e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.539612e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00141522 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00148606 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 1.083059e+00 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.00169962 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.0017581 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 1.015944e+00 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OMP method
Time CPU total OMP Events: 300 Tracks: 95876 Hits: 598963 = 2.274990e-03 s

-----------------------------
