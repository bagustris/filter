Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.428127e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.140800e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.700920e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 6.883390e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.110400e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.080036e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.843162e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 5.507469e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 4.6432e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.39098e-05 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 7.283070e-01 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 5.2672e-05 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 8.10623e-05 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 7.207770e-01 s

-----------------------------
Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.268148e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.129600e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.089573e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.848011e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.362240e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.327991e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.819179e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 5.810261e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.4624e-05 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000104904 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 7.287030e-01 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.296e-05 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000100851 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 7.213140e-01 s

-----------------------------
Test for Bad
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with Bad method
Time CPU total Bad Events: 21 Tracks: 6930 Hits: 42258 = 2.136946e-03 s

Test for Good
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 21 Tracks: 6930 Hits: 42258 = 1.549120e-04 s
Time CPU Kernel Cuda2 Events: 21 Tracks: 6930 Hits: 42258 = 1.490116e-04 s
Time CPU total Cuda2 Events: 21 Tracks: 6930 Hits: 42258 = 6.875479e-01 s

Test for Good
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 21 Tracks: 6930 Hits: 42258 = 1.822400e-04 s
Time CPU Kernel Cuda1 Events: 21 Tracks: 6930 Hits: 42258 = 1.769066e-04 s
Time CPU total Cuda1 Events: 21 Tracks: 6930 Hits: 42258 = 6.823492e-01 s

Test for Good
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with Good method
Time CPU total Good Events: 21 Tracks: 6930 Hits: 42258 = 1.110077e-03 s

Test for OpenCL
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 21 Tracks: 6930 Hits: 42258 = 0.000109024 s
Time CPU Kernel OpenCl2 Events: 21 Tracks: 6930 Hits: 42258 = 0.000263929 s
Time CPU total OpenCl2 Events: 21 Tracks: 6930 Hits: 42258 = 7.257679e-01 s

Test for OpenCL
To import-> Hits: 42258, Tracks: 6930 Events: 21
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 21 Tracks: 6930 Hits: 42258 = 0.000109696 s
Time CPU Kernel OpenCl1 Events: 21 Tracks: 6930 Hits: 42258 = 0.000264883 s
Time CPU total OpenCl1 Events: 21 Tracks: 6930 Hits: 42258 = 7.228129e-01 s

-----------------------------
Test for Bad
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with Bad method
Time CPU total Bad Events: 31 Tracks: 9564 Hits: 58986 = 3.312111e-03 s

Test for Good
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 31 Tracks: 9564 Hits: 58986 = 1.840640e-04 s
Time CPU Kernel Cuda2 Events: 31 Tracks: 9564 Hits: 58986 = 1.778603e-04 s
Time CPU total Cuda2 Events: 31 Tracks: 9564 Hits: 58986 = 6.866629e-01 s

Test for Good
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 31 Tracks: 9564 Hits: 58986 = 2.215040e-04 s
Time CPU Kernel Cuda1 Events: 31 Tracks: 9564 Hits: 58986 = 2.169609e-04 s
Time CPU total Cuda1 Events: 31 Tracks: 9564 Hits: 58986 = 6.910419e-01 s

Test for Good
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with Good method
Time CPU total Good Events: 31 Tracks: 9564 Hits: 58986 = 1.549959e-03 s

Test for OpenCL
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 31 Tracks: 9564 Hits: 58986 = 0.00014144 s
Time CPU Kernel OpenCl2 Events: 31 Tracks: 9564 Hits: 58986 = 0.000169992 s
Time CPU total OpenCl2 Events: 31 Tracks: 9564 Hits: 58986 = 7.254310e-01 s

Test for OpenCL
To import-> Hits: 58986, Tracks: 9564 Events: 31
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 31 Tracks: 9564 Hits: 58986 = 0.000131104 s
Time CPU Kernel OpenCl1 Events: 31 Tracks: 9564 Hits: 58986 = 0.000159025 s
Time CPU total OpenCl1 Events: 31 Tracks: 9564 Hits: 58986 = 7.230501e-01 s

-----------------------------
Test for Bad
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with Bad method
Time CPU total Bad Events: 41 Tracks: 12395 Hits: 77259 = 3.779888e-03 s

Test for Good
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 41 Tracks: 12395 Hits: 77259 = 2.112000e-04 s
Time CPU Kernel Cuda2 Events: 41 Tracks: 12395 Hits: 77259 = 2.081394e-04 s
Time CPU total Cuda2 Events: 41 Tracks: 12395 Hits: 77259 = 6.878500e-01 s

Test for Good
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 41 Tracks: 12395 Hits: 77259 = 2.540480e-04 s
Time CPU Kernel Cuda1 Events: 41 Tracks: 12395 Hits: 77259 = 2.510548e-04 s
Time CPU total Cuda1 Events: 41 Tracks: 12395 Hits: 77259 = 6.841261e-01 s

Test for Good
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with Good method
Time CPU total Good Events: 41 Tracks: 12395 Hits: 77259 = 2.058983e-03 s

Test for OpenCL
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 41 Tracks: 12395 Hits: 77259 = 0.00017616 s
Time CPU Kernel OpenCl2 Events: 41 Tracks: 12395 Hits: 77259 = 0.000204086 s
Time CPU total OpenCl2 Events: 41 Tracks: 12395 Hits: 77259 = 7.224858e-01 s

Test for OpenCL
To import-> Hits: 77259, Tracks: 12395 Events: 41
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 41 Tracks: 12395 Hits: 77259 = 0.000153792 s
Time CPU Kernel OpenCl1 Events: 41 Tracks: 12395 Hits: 77259 = 0.000181913 s
Time CPU total OpenCl1 Events: 41 Tracks: 12395 Hits: 77259 = 7.210689e-01 s

-----------------------------
Test for Bad
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with Bad method
Time CPU total Bad Events: 51 Tracks: 15313 Hits: 95633 = 4.939079e-03 s

Test for Good
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 51 Tracks: 15313 Hits: 95633 = 2.323840e-04 s
Time CPU Kernel Cuda2 Events: 51 Tracks: 15313 Hits: 95633 = 2.298355e-04 s
Time CPU total Cuda2 Events: 51 Tracks: 15313 Hits: 95633 = 6.906888e-01 s

Test for Good
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 51 Tracks: 15313 Hits: 95633 = 2.910080e-04 s
Time CPU Kernel Cuda1 Events: 51 Tracks: 15313 Hits: 95633 = 2.889633e-04 s
Time CPU total Cuda1 Events: 51 Tracks: 15313 Hits: 95633 = 6.860900e-01 s

Test for Good
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with Good method
Time CPU total Good Events: 51 Tracks: 15313 Hits: 95633 = 2.509117e-03 s

Test for OpenCL
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 51 Tracks: 15313 Hits: 95633 = 0.000203968 s
Time CPU Kernel OpenCl2 Events: 51 Tracks: 15313 Hits: 95633 = 0.000231028 s
Time CPU total OpenCl2 Events: 51 Tracks: 15313 Hits: 95633 = 7.242260e-01 s

Test for OpenCL
To import-> Hits: 95633, Tracks: 15313 Events: 51
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 51 Tracks: 15313 Hits: 95633 = 0.00017584 s
Time CPU Kernel OpenCl1 Events: 51 Tracks: 15313 Hits: 95633 = 0.000203848 s
Time CPU total OpenCl1 Events: 51 Tracks: 15313 Hits: 95633 = 7.252491e-01 s

-----------------------------
Test for Bad
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with Bad method
Time CPU total Bad Events: 61 Tracks: 18714 Hits: 116855 = 6.767988e-03 s

Test for Good
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 61 Tracks: 18714 Hits: 116855 = 2.568640e-04 s
Time CPU Kernel Cuda2 Events: 61 Tracks: 18714 Hits: 116855 = 2.548695e-04 s
Time CPU total Cuda2 Events: 61 Tracks: 18714 Hits: 116855 = 6.561329e-01 s

Test for Good
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 61 Tracks: 18714 Hits: 116855 = 3.265600e-04 s
Time CPU Kernel Cuda1 Events: 61 Tracks: 18714 Hits: 116855 = 3.252029e-04 s
Time CPU total Cuda1 Events: 61 Tracks: 18714 Hits: 116855 = 6.798069e-01 s

Test for Good
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with Good method
Time CPU total Good Events: 61 Tracks: 18714 Hits: 116855 = 3.094912e-03 s

Test for OpenCL
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 61 Tracks: 18714 Hits: 116855 = 0.000232704 s
Time CPU Kernel OpenCl2 Events: 61 Tracks: 18714 Hits: 116855 = 0.000393867 s
Time CPU total OpenCl2 Events: 61 Tracks: 18714 Hits: 116855 = 7.274349e-01 s

Test for OpenCL
To import-> Hits: 116855, Tracks: 18714 Events: 61
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 61 Tracks: 18714 Hits: 116855 = 0.000201376 s
Time CPU Kernel OpenCl1 Events: 61 Tracks: 18714 Hits: 116855 = 0.000359058 s
Time CPU total OpenCl1 Events: 61 Tracks: 18714 Hits: 116855 = 7.324040e-01 s

-----------------------------
Test for Bad
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with Bad method
Time CPU total Bad Events: 71 Tracks: 21960 Hits: 136914 = 6.552219e-03 s

Test for Good
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 71 Tracks: 21960 Hits: 136914 = 2.878080e-04 s
Time CPU Kernel Cuda2 Events: 71 Tracks: 21960 Hits: 136914 = 2.880096e-04 s
Time CPU total Cuda2 Events: 71 Tracks: 21960 Hits: 136914 = 7.017100e-01 s

Test for Good
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 71 Tracks: 21960 Hits: 136914 = 3.574400e-04 s
Time CPU Kernel Cuda1 Events: 71 Tracks: 21960 Hits: 136914 = 3.569126e-04 s
Time CPU total Cuda1 Events: 71 Tracks: 21960 Hits: 136914 = 6.942260e-01 s

Test for Good
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with Good method
Time CPU total Good Events: 71 Tracks: 21960 Hits: 136914 = 3.655910e-03 s

Test for OpenCL
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 71 Tracks: 21960 Hits: 136914 = 0.000269952 s
Time CPU Kernel OpenCl2 Events: 71 Tracks: 21960 Hits: 136914 = 0.000422955 s
Time CPU total OpenCl2 Events: 71 Tracks: 21960 Hits: 136914 = 7.313118e-01 s

Test for OpenCL
To import-> Hits: 136914, Tracks: 21960 Events: 71
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 71 Tracks: 21960 Hits: 136914 = 0.000223744 s
Time CPU Kernel OpenCl1 Events: 71 Tracks: 21960 Hits: 136914 = 0.000380039 s
Time CPU total OpenCl1 Events: 71 Tracks: 21960 Hits: 136914 = 7.308362e-01 s

-----------------------------
Test for Bad
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with Bad method
Time CPU total Bad Events: 81 Tracks: 25224 Hits: 157605 = 7.935047e-03 s

Test for Good
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 81 Tracks: 25224 Hits: 157605 = 3.130880e-04 s
Time CPU Kernel Cuda2 Events: 81 Tracks: 25224 Hits: 157605 = 3.139973e-04 s
Time CPU total Cuda2 Events: 81 Tracks: 25224 Hits: 157605 = 6.872900e-01 s

Test for Good
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 81 Tracks: 25224 Hits: 157605 = 4.018560e-04 s
Time CPU Kernel Cuda1 Events: 81 Tracks: 25224 Hits: 157605 = 4.050732e-04 s
Time CPU total Cuda1 Events: 81 Tracks: 25224 Hits: 157605 = 6.881011e-01 s

Test for Good
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with Good method
Time CPU total Good Events: 81 Tracks: 25224 Hits: 157605 = 4.184008e-03 s

Test for OpenCL
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 81 Tracks: 25224 Hits: 157605 = 0.000307488 s
Time CPU Kernel OpenCl2 Events: 81 Tracks: 25224 Hits: 157605 = 0.000463963 s
Time CPU total OpenCl2 Events: 81 Tracks: 25224 Hits: 157605 = 7.317488e-01 s

Test for OpenCL
To import-> Hits: 157605, Tracks: 25224 Events: 81
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 81 Tracks: 25224 Hits: 157605 = 0.000252096 s
Time CPU Kernel OpenCl1 Events: 81 Tracks: 25224 Hits: 157605 = 0.000408173 s
Time CPU total OpenCl1 Events: 81 Tracks: 25224 Hits: 157605 = 7.364142e-01 s

-----------------------------
Test for Bad
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with Bad method
Time CPU total Bad Events: 91 Tracks: 28711 Hits: 179208 = 9.534121e-03 s

Test for Good
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 91 Tracks: 28711 Hits: 179208 = 3.460160e-04 s
Time CPU Kernel Cuda2 Events: 91 Tracks: 28711 Hits: 179208 = 3.490448e-04 s
Time CPU total Cuda2 Events: 91 Tracks: 28711 Hits: 179208 = 6.976922e-01 s

Test for Good
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 91 Tracks: 28711 Hits: 179208 = 4.470720e-04 s
Time CPU Kernel Cuda1 Events: 91 Tracks: 28711 Hits: 179208 = 4.489422e-04 s
Time CPU total Cuda1 Events: 91 Tracks: 28711 Hits: 179208 = 6.957879e-01 s

Test for Good
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with Good method
Time CPU total Good Events: 91 Tracks: 28711 Hits: 179208 = 4.747152e-03 s

Test for OpenCL
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 91 Tracks: 28711 Hits: 179208 = 0.000339872 s
Time CPU Kernel OpenCl2 Events: 91 Tracks: 28711 Hits: 179208 = 0.000494957 s
Time CPU total OpenCl2 Events: 91 Tracks: 28711 Hits: 179208 = 7.361701e-01 s

Test for OpenCL
To import-> Hits: 179208, Tracks: 28711 Events: 91
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 91 Tracks: 28711 Hits: 179208 = 0.000285056 s
Time CPU Kernel OpenCl1 Events: 91 Tracks: 28711 Hits: 179208 = 0.000441074 s
Time CPU total OpenCl1 Events: 91 Tracks: 28711 Hits: 179208 = 7.322230e-01 s

-----------------------------
Test for Bad
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with Bad method
Time CPU total Bad Events: 101 Tracks: 31888 Hits: 198857 = 1.090980e-02 s

Test for Good
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 101 Tracks: 31888 Hits: 198857 = 3.676160e-04 s
Time CPU Kernel Cuda2 Events: 101 Tracks: 31888 Hits: 198857 = 3.709793e-04 s
Time CPU total Cuda2 Events: 101 Tracks: 31888 Hits: 198857 = 6.939721e-01 s

Test for Good
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 101 Tracks: 31888 Hits: 198857 = 4.701440e-04 s
Time CPU Kernel Cuda1 Events: 101 Tracks: 31888 Hits: 198857 = 4.720688e-04 s
Time CPU total Cuda1 Events: 101 Tracks: 31888 Hits: 198857 = 6.911728e-01 s

Test for Good
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with Good method
Time CPU total Good Events: 101 Tracks: 31888 Hits: 198857 = 5.222082e-03 s

Test for OpenCL
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 101 Tracks: 31888 Hits: 198857 = 0.000376448 s
Time CPU Kernel OpenCl2 Events: 101 Tracks: 31888 Hits: 198857 = 0.000529051 s
Time CPU total OpenCl2 Events: 101 Tracks: 31888 Hits: 198857 = 7.237611e-01 s

Test for OpenCL
To import-> Hits: 198857, Tracks: 31888 Events: 101
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 101 Tracks: 31888 Hits: 198857 = 0.000300512 s
Time CPU Kernel OpenCl1 Events: 101 Tracks: 31888 Hits: 198857 = 0.000453949 s
Time CPU total OpenCl1 Events: 101 Tracks: 31888 Hits: 198857 = 7.284520e-01 s

-----------------------------
Test for Bad
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with Bad method
Time CPU total Bad Events: 111 Tracks: 34704 Hits: 216669 = 1.225495e-02 s

Test for Good
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 111 Tracks: 34704 Hits: 216669 = 3.951360e-04 s
Time CPU Kernel Cuda2 Events: 111 Tracks: 34704 Hits: 216669 = 4.000664e-04 s
Time CPU total Cuda2 Events: 111 Tracks: 34704 Hits: 216669 = 6.823010e-01 s

Test for Good
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 111 Tracks: 34704 Hits: 216669 = 5.091840e-04 s
Time CPU Kernel Cuda1 Events: 111 Tracks: 34704 Hits: 216669 = 5.130768e-04 s
Time CPU total Cuda1 Events: 111 Tracks: 34704 Hits: 216669 = 6.903059e-01 s

Test for Good
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with Good method
Time CPU total Good Events: 111 Tracks: 34704 Hits: 216669 = 5.748987e-03 s

Test for OpenCL
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 111 Tracks: 34704 Hits: 216669 = 0.000394976 s
Time CPU Kernel OpenCl2 Events: 111 Tracks: 34704 Hits: 216669 = 0.000551939 s
Time CPU total OpenCl2 Events: 111 Tracks: 34704 Hits: 216669 = 7.337499e-01 s

Test for OpenCL
To import-> Hits: 216669, Tracks: 34704 Events: 111
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 111 Tracks: 34704 Hits: 216669 = 0.000324352 s
Time CPU Kernel OpenCl1 Events: 111 Tracks: 34704 Hits: 216669 = 0.000482082 s
Time CPU total OpenCl1 Events: 111 Tracks: 34704 Hits: 216669 = 7.351229e-01 s

-----------------------------
Test for Bad
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with Bad method
Time CPU total Bad Events: 121 Tracks: 38044 Hits: 237797 = 1.361918e-02 s

Test for Good
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 121 Tracks: 38044 Hits: 237797 = 4.292800e-04 s
Time CPU Kernel Cuda2 Events: 121 Tracks: 38044 Hits: 237797 = 4.341602e-04 s
Time CPU total Cuda2 Events: 121 Tracks: 38044 Hits: 237797 = 7.061930e-01 s

Test for Good
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 121 Tracks: 38044 Hits: 237797 = 5.580480e-04 s
Time CPU Kernel Cuda1 Events: 121 Tracks: 38044 Hits: 237797 = 5.631447e-04 s
Time CPU total Cuda1 Events: 121 Tracks: 38044 Hits: 237797 = 6.916409e-01 s

Test for Good
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with Good method
Time CPU total Good Events: 121 Tracks: 38044 Hits: 237797 = 6.304026e-03 s

Test for OpenCL
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 121 Tracks: 38044 Hits: 237797 = 0.00042864 s
Time CPU Kernel OpenCl2 Events: 121 Tracks: 38044 Hits: 237797 = 0.000584126 s
Time CPU total OpenCl2 Events: 121 Tracks: 38044 Hits: 237797 = 7.474461e-01 s

Test for OpenCL
To import-> Hits: 237797, Tracks: 38044 Events: 121
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 121 Tracks: 38044 Hits: 237797 = 0.00035648 s
Time CPU Kernel OpenCl1 Events: 121 Tracks: 38044 Hits: 237797 = 0.000513077 s
Time CPU total OpenCl1 Events: 121 Tracks: 38044 Hits: 237797 = 7.349131e-01 s

-----------------------------
Test for Bad
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with Bad method
Time CPU total Bad Events: 131 Tracks: 41195 Hits: 257010 = 1.194000e-02 s

Test for Good
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 131 Tracks: 41195 Hits: 257010 = 4.520000e-04 s
Time CPU Kernel Cuda2 Events: 131 Tracks: 41195 Hits: 257010 = 4.570484e-04 s
Time CPU total Cuda2 Events: 131 Tracks: 41195 Hits: 257010 = 6.950078e-01 s

Test for Good
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 131 Tracks: 41195 Hits: 257010 = 5.909440e-04 s
Time CPU Kernel Cuda1 Events: 131 Tracks: 41195 Hits: 257010 = 5.981922e-04 s
Time CPU total Cuda1 Events: 131 Tracks: 41195 Hits: 257010 = 6.964140e-01 s

Test for Good
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with Good method
Time CPU total Good Events: 131 Tracks: 41195 Hits: 257010 = 6.788015e-03 s

Test for OpenCL
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 131 Tracks: 41195 Hits: 257010 = 0.000480864 s
Time CPU Kernel OpenCl2 Events: 131 Tracks: 41195 Hits: 257010 = 0.000637054 s
Time CPU total OpenCl2 Events: 131 Tracks: 41195 Hits: 257010 = 7.521160e-01 s

Test for OpenCL
To import-> Hits: 257010, Tracks: 41195 Events: 131
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 131 Tracks: 41195 Hits: 257010 = 0.000375872 s
Time CPU Kernel OpenCl1 Events: 131 Tracks: 41195 Hits: 257010 = 0.000534058 s
Time CPU total OpenCl1 Events: 131 Tracks: 41195 Hits: 257010 = 7.338319e-01 s

-----------------------------
Test for Bad
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with Bad method
Time CPU total Bad Events: 141 Tracks: 45001 Hits: 280694 = 1.324415e-02 s

Test for Good
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 141 Tracks: 45001 Hits: 280694 = 4.932800e-04 s
Time CPU Kernel Cuda2 Events: 141 Tracks: 45001 Hits: 280694 = 5.009174e-04 s
Time CPU total Cuda2 Events: 141 Tracks: 45001 Hits: 280694 = 6.957321e-01 s

Test for Good
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 141 Tracks: 45001 Hits: 280694 = 6.387520e-04 s
Time CPU Kernel Cuda1 Events: 141 Tracks: 45001 Hits: 280694 = 6.461143e-04 s
Time CPU total Cuda1 Events: 141 Tracks: 45001 Hits: 280694 = 6.971281e-01 s

Test for Good
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with Good method
Time CPU total Good Events: 141 Tracks: 45001 Hits: 280694 = 7.447004e-03 s

Test for OpenCL
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 141 Tracks: 45001 Hits: 280694 = 0.00052432 s
Time CPU Kernel OpenCl2 Events: 141 Tracks: 45001 Hits: 280694 = 0.000682116 s
Time CPU total OpenCl2 Events: 141 Tracks: 45001 Hits: 280694 = 7.327089e-01 s

Test for OpenCL
To import-> Hits: 280694, Tracks: 45001 Events: 141
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 141 Tracks: 45001 Hits: 280694 = 0.000411808 s
Time CPU Kernel OpenCl1 Events: 141 Tracks: 45001 Hits: 280694 = 0.000572205 s
Time CPU total OpenCl1 Events: 141 Tracks: 45001 Hits: 280694 = 7.360971e-01 s

-----------------------------
Test for Bad
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with Bad method
Time CPU total Bad Events: 151 Tracks: 48329 Hits: 301352 = 1.431298e-02 s

Test for Good
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 151 Tracks: 48329 Hits: 301352 = 5.178881e-04 s
Time CPU Kernel Cuda2 Events: 151 Tracks: 48329 Hits: 301352 = 5.271435e-04 s
Time CPU total Cuda2 Events: 151 Tracks: 48329 Hits: 301352 = 6.977592e-01 s

Test for Good
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 151 Tracks: 48329 Hits: 301352 = 6.700800e-04 s
Time CPU Kernel Cuda1 Events: 151 Tracks: 48329 Hits: 301352 = 6.790161e-04 s
Time CPU total Cuda1 Events: 151 Tracks: 48329 Hits: 301352 = 6.954060e-01 s

Test for Good
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with Good method
Time CPU total Good Events: 151 Tracks: 48329 Hits: 301352 = 7.888079e-03 s

Test for OpenCL
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 151 Tracks: 48329 Hits: 301352 = 0.00054816 s
Time CPU Kernel OpenCl2 Events: 151 Tracks: 48329 Hits: 301352 = 0.00070405 s
Time CPU total OpenCl2 Events: 151 Tracks: 48329 Hits: 301352 = 7.391860e-01 s

Test for OpenCL
To import-> Hits: 301352, Tracks: 48329 Events: 151
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 151 Tracks: 48329 Hits: 301352 = 0.000434848 s
Time CPU Kernel OpenCl1 Events: 151 Tracks: 48329 Hits: 301352 = 0.000591993 s
Time CPU total OpenCl1 Events: 151 Tracks: 48329 Hits: 301352 = 7.347069e-01 s

-----------------------------
Test for Bad
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with Bad method
Time CPU total Bad Events: 161 Tracks: 51978 Hits: 323567 = 1.593208e-02 s

Test for Good
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 161 Tracks: 51978 Hits: 323567 = 5.336000e-04 s
Time CPU Kernel Cuda2 Events: 161 Tracks: 51978 Hits: 323567 = 5.431175e-04 s
Time CPU total Cuda2 Events: 161 Tracks: 51978 Hits: 323567 = 7.012930e-01 s

Test for Good
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 161 Tracks: 51978 Hits: 323567 = 7.080961e-04 s
Time CPU Kernel Cuda1 Events: 161 Tracks: 51978 Hits: 323567 = 7.181168e-04 s
Time CPU total Cuda1 Events: 161 Tracks: 51978 Hits: 323567 = 6.952891e-01 s

Test for Good
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with Good method
Time CPU total Good Events: 161 Tracks: 51978 Hits: 323567 = 8.589983e-03 s

Test for OpenCL
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 161 Tracks: 51978 Hits: 323567 = 0.000606656 s
Time CPU Kernel OpenCl2 Events: 161 Tracks: 51978 Hits: 323567 = 0.000752926 s
Time CPU total OpenCl2 Events: 161 Tracks: 51978 Hits: 323567 = 7.236080e-01 s

Test for OpenCL
To import-> Hits: 323567, Tracks: 51978 Events: 161
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 161 Tracks: 51978 Hits: 323567 = 0.000463104 s
Time CPU Kernel OpenCl1 Events: 161 Tracks: 51978 Hits: 323567 = 0.000615835 s
Time CPU total OpenCl1 Events: 161 Tracks: 51978 Hits: 323567 = 7.294922e-01 s

-----------------------------
Test for Bad
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with Bad method
Time CPU total Bad Events: 171 Tracks: 54846 Hits: 341879 = 1.708603e-02 s

Test for Good
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 171 Tracks: 54846 Hits: 341879 = 5.752320e-04 s
Time CPU Kernel Cuda2 Events: 171 Tracks: 54846 Hits: 341879 = 5.857944e-04 s
Time CPU total Cuda2 Events: 171 Tracks: 54846 Hits: 341879 = 6.977541e-01 s

Test for Good
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 171 Tracks: 54846 Hits: 341879 = 7.482560e-04 s
Time CPU Kernel Cuda1 Events: 171 Tracks: 54846 Hits: 341879 = 7.550716e-04 s
Time CPU total Cuda1 Events: 171 Tracks: 54846 Hits: 341879 = 6.959791e-01 s

Test for Good
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with Good method
Time CPU total Good Events: 171 Tracks: 54846 Hits: 341879 = 9.082794e-03 s

Test for OpenCL
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 171 Tracks: 54846 Hits: 341879 = 0.000607936 s
Time CPU Kernel OpenCl2 Events: 171 Tracks: 54846 Hits: 341879 = 0.000756979 s
Time CPU total OpenCl2 Events: 171 Tracks: 54846 Hits: 341879 = 7.335241e-01 s

Test for OpenCL
To import-> Hits: 341879, Tracks: 54846 Events: 171
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 171 Tracks: 54846 Hits: 341879 = 0.000478688 s
Time CPU Kernel OpenCl1 Events: 171 Tracks: 54846 Hits: 341879 = 0.000634909 s
Time CPU total OpenCl1 Events: 171 Tracks: 54846 Hits: 341879 = 7.433460e-01 s

-----------------------------
Test for Bad
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with Bad method
Time CPU total Bad Events: 181 Tracks: 57590 Hits: 359113 = 1.827002e-02 s

Test for Good
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 181 Tracks: 57590 Hits: 359113 = 5.863680e-04 s
Time CPU Kernel Cuda2 Events: 181 Tracks: 57590 Hits: 359113 = 5.970001e-04 s
Time CPU total Cuda2 Events: 181 Tracks: 57590 Hits: 359113 = 7.030351e-01 s

Test for Good
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 181 Tracks: 57590 Hits: 359113 = 7.793281e-04 s
Time CPU Kernel Cuda1 Events: 181 Tracks: 57590 Hits: 359113 = 7.910728e-04 s
Time CPU total Cuda1 Events: 181 Tracks: 57590 Hits: 359113 = 7.044160e-01 s

Test for Good
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with Good method
Time CPU total Good Events: 181 Tracks: 57590 Hits: 359113 = 9.523869e-03 s

Test for OpenCL
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 181 Tracks: 57590 Hits: 359113 = 0.000642016 s
Time CPU Kernel OpenCl2 Events: 181 Tracks: 57590 Hits: 359113 = 0.000791073 s
Time CPU total OpenCl2 Events: 181 Tracks: 57590 Hits: 359113 = 7.515819e-01 s

Test for OpenCL
To import-> Hits: 359113, Tracks: 57590 Events: 181
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 181 Tracks: 57590 Hits: 359113 = 0.000507104 s
Time CPU Kernel OpenCl1 Events: 181 Tracks: 57590 Hits: 359113 = 0.000659943 s
Time CPU total OpenCl1 Events: 181 Tracks: 57590 Hits: 359113 = 7.423720e-01 s

-----------------------------
Test for Bad
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with Bad method
Time CPU total Bad Events: 191 Tracks: 60753 Hits: 378563 = 1.977992e-02 s

Test for Good
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 191 Tracks: 60753 Hits: 378563 = 6.226240e-04 s
Time CPU Kernel Cuda2 Events: 191 Tracks: 60753 Hits: 378563 = 6.361008e-04 s
Time CPU total Cuda2 Events: 191 Tracks: 60753 Hits: 378563 = 6.994200e-01 s

Test for Good
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 191 Tracks: 60753 Hits: 378563 = 8.232641e-04 s
Time CPU Kernel Cuda1 Events: 191 Tracks: 60753 Hits: 378563 = 8.370876e-04 s
Time CPU total Cuda1 Events: 191 Tracks: 60753 Hits: 378563 = 6.952181e-01 s

Test for Good
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with Good method
Time CPU total Good Events: 191 Tracks: 60753 Hits: 378563 = 1.002097e-02 s

Test for OpenCL
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 191 Tracks: 60753 Hits: 378563 = 0.000669312 s
Time CPU Kernel OpenCl2 Events: 191 Tracks: 60753 Hits: 378563 = 0.000818968 s
Time CPU total OpenCl2 Events: 191 Tracks: 60753 Hits: 378563 = 7.374570e-01 s

Test for OpenCL
To import-> Hits: 378563, Tracks: 60753 Events: 191
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 191 Tracks: 60753 Hits: 378563 = 0.000527776 s
Time CPU Kernel OpenCl1 Events: 191 Tracks: 60753 Hits: 378563 = 0.00067997 s
Time CPU total OpenCl1 Events: 191 Tracks: 60753 Hits: 378563 = 7.327249e-01 s

-----------------------------
