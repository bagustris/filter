Test for Bad
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Bad method
Time CPU total Bad Events: 1 Tracks: 358 Hits: 2093 = 1.518726e-04 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 8.166401e-05 s
Time CPU Kernel Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 7.796288e-05 s
Time CPU total Cuda2 Events: 1 Tracks: 358 Hits: 2093 = 6.983552e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.096640e-04 s
Time CPU Kernel Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 1.060963e-04 s
Time CPU total Cuda1 Events: 1 Tracks: 358 Hits: 2093 = 6.941690e-01 s

Test for Good
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with Good method
Time CPU total Good Events: 1 Tracks: 358 Hits: 2093 = 5.483627e-05 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 9.2767e-05 s
Time CPU Kernel OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 0.000142097 s
Time CPU total OpenCl2 Events: 1 Tracks: 358 Hits: 2093 = 1.076571e+00 s

Test for OpenCL
To import-> Hits: 2093, Tracks: 358 Events: 1
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 0.000124948 s
Time CPU Kernel OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 0.000178099 s
Time CPU total OpenCl1 Events: 1 Tracks: 358 Hits: 2093 = 9.996941e-01 s

-----------------------------
Test for Bad
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Bad method
Time CPU total Bad Events: 2 Tracks: 532 Hits: 3158 = 1.988411e-04 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 8.278400e-05 s
Time CPU Kernel Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 7.891655e-05 s
Time CPU total Cuda2 Events: 2 Tracks: 532 Hits: 3158 = 6.866510e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.120640e-04 s
Time CPU Kernel Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 1.080036e-04 s
Time CPU total Cuda1 Events: 2 Tracks: 532 Hits: 3158 = 6.877699e-01 s

Test for Good
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with Good method
Time CPU total Good Events: 2 Tracks: 532 Hits: 3158 = 8.201599e-05 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 0.000122559 s
Time CPU Kernel OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 0.000170946 s
Time CPU total OpenCl2 Events: 2 Tracks: 532 Hits: 3158 = 1.088751e+00 s

Test for OpenCL
To import-> Hits: 3158, Tracks: 532 Events: 2
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 0.000122754 s
Time CPU Kernel OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 0.0001719 s
Time CPU total OpenCl1 Events: 2 Tracks: 532 Hits: 3158 = 9.911189e-01 s

-----------------------------
Test for Bad
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Bad method
Time CPU total Bad Events: 3 Tracks: 779 Hits: 4741 = 2.639294e-04 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 8.246401e-05 s
Time CPU Kernel Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 7.987022e-05 s
Time CPU total Cuda2 Events: 3 Tracks: 779 Hits: 4741 = 6.807711e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.120320e-04 s
Time CPU Kernel Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 1.089573e-04 s
Time CPU total Cuda1 Events: 3 Tracks: 779 Hits: 4741 = 6.899271e-01 s

Test for Good
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with Good method
Time CPU total Good Events: 3 Tracks: 779 Hits: 4741 = 1.239777e-04 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 0.000110956 s
Time CPU Kernel OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 0.000157833 s
Time CPU total OpenCl2 Events: 3 Tracks: 779 Hits: 4741 = 1.046691e+00 s

Test for OpenCL
To import-> Hits: 4741, Tracks: 779 Events: 3
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 0.000132959 s
Time CPU Kernel OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 0.000184059 s
Time CPU total OpenCl1 Events: 3 Tracks: 779 Hits: 4741 = 1.004790e+00 s

-----------------------------
Test for Bad
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Bad method
Time CPU total Bad Events: 4 Tracks: 1300 Hits: 8061 = 5.018711e-04 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.892801e-05 s
Time CPU Kernel Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 8.487701e-05 s
Time CPU total Cuda2 Events: 4 Tracks: 1300 Hits: 8061 = 6.844521e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.174080e-04 s
Time CPU Kernel Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 1.127720e-04 s
Time CPU total Cuda1 Events: 4 Tracks: 1300 Hits: 8061 = 6.860900e-01 s

Test for Good
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with Good method
Time CPU total Good Events: 4 Tracks: 1300 Hits: 8061 = 2.131462e-04 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 0.000166149 s
Time CPU Kernel OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 0.000231028 s
Time CPU total OpenCl2 Events: 4 Tracks: 1300 Hits: 8061 = 1.064106e+00 s

Test for OpenCL
To import-> Hits: 8061, Tracks: 1300 Events: 4
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 0.000118911 s
Time CPU Kernel OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 0.000172138 s
Time CPU total OpenCl1 Events: 4 Tracks: 1300 Hits: 8061 = 1.000081e+00 s

-----------------------------
Test for Bad
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Bad method
Time CPU total Bad Events: 5 Tracks: 1914 Hits: 11850 = 7.710457e-04 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 1.151360e-04 s
Time CPU Kernel Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 8.702278e-05 s
Time CPU total Cuda2 Events: 5 Tracks: 1914 Hits: 11850 = 6.898530e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.149120e-04 s
Time CPU Kernel Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 1.118183e-04 s
Time CPU total Cuda1 Events: 5 Tracks: 1914 Hits: 11850 = 6.880729e-01 s

Test for Good
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with Good method
Time CPU total Good Events: 5 Tracks: 1914 Hits: 11850 = 3.159046e-04 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 0.000229725 s
Time CPU Kernel OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 0.000281096 s
Time CPU total OpenCl2 Events: 5 Tracks: 1914 Hits: 11850 = 1.084718e+00 s

Test for OpenCL
To import-> Hits: 11850, Tracks: 1914 Events: 5
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 0.000208789 s
Time CPU Kernel OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 0.000257969 s
Time CPU total OpenCl1 Events: 5 Tracks: 1914 Hits: 11850 = 1.007330e+00 s

-----------------------------
Test for Bad
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Bad method
Time CPU total Bad Events: 6 Tracks: 2089 Hits: 12849 = 8.039474e-04 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 9.321600e-05 s
Time CPU Kernel Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 8.916855e-05 s
Time CPU total Cuda2 Events: 6 Tracks: 2089 Hits: 12849 = 6.865950e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.175680e-04 s
Time CPU Kernel Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 1.142025e-04 s
Time CPU total Cuda1 Events: 6 Tracks: 2089 Hits: 12849 = 6.579869e-01 s

Test for Good
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with Good method
Time CPU total Good Events: 6 Tracks: 2089 Hits: 12849 = 3.409386e-04 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 0.000147081 s
Time CPU Kernel OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 0.000195026 s
Time CPU total OpenCl2 Events: 6 Tracks: 2089 Hits: 12849 = 1.010230e+00 s

Test for OpenCL
To import-> Hits: 12849, Tracks: 2089 Events: 6
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 0.000224143 s
Time CPU Kernel OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 0.000275135 s
Time CPU total OpenCl1 Events: 6 Tracks: 2089 Hits: 12849 = 1.004007e+00 s

-----------------------------
Test for Bad
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Bad method
Time CPU total Bad Events: 7 Tracks: 2418 Hits: 14812 = 9.701252e-04 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 1.011840e-04 s
Time CPU Kernel Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 9.703636e-05 s
Time CPU total Cuda2 Events: 7 Tracks: 2418 Hits: 14812 = 6.889460e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.257280e-04 s
Time CPU Kernel Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 1.218319e-04 s
Time CPU total Cuda1 Events: 7 Tracks: 2418 Hits: 14812 = 6.830959e-01 s

Test for Good
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with Good method
Time CPU total Good Events: 7 Tracks: 2418 Hits: 14812 = 3.709793e-04 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 0.000198706 s
Time CPU Kernel OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 0.000243187 s
Time CPU total OpenCl2 Events: 7 Tracks: 2418 Hits: 14812 = 1.070856e+00 s

Test for OpenCL
To import-> Hits: 14812, Tracks: 2418 Events: 7
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 0.00017071 s
Time CPU Kernel OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 0.000221014 s
Time CPU total OpenCl1 Events: 7 Tracks: 2418 Hits: 14812 = 1.019733e+00 s

-----------------------------
Test for Bad
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Bad method
Time CPU total Bad Events: 8 Tracks: 2586 Hits: 15814 = 1.008987e-03 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.104000e-04 s
Time CPU Kernel Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 1.060963e-04 s
Time CPU total Cuda2 Events: 8 Tracks: 2586 Hits: 15814 = 6.856451e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.299200e-04 s
Time CPU Kernel Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 1.258850e-04 s
Time CPU total Cuda1 Events: 8 Tracks: 2586 Hits: 15814 = 6.846249e-01 s

Test for Good
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with Good method
Time CPU total Good Events: 8 Tracks: 2586 Hits: 15814 = 4.010201e-04 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 0.000171303 s
Time CPU Kernel OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 0.000232935 s
Time CPU total OpenCl2 Events: 8 Tracks: 2586 Hits: 15814 = 1.149283e+00 s

Test for OpenCL
To import-> Hits: 15814, Tracks: 2586 Events: 8
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.000185104 s
Time CPU Kernel OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 0.000239134 s
Time CPU total OpenCl1 Events: 8 Tracks: 2586 Hits: 15814 = 1.032309e+00 s

-----------------------------
Test for Bad
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Bad method
Time CPU total Bad Events: 9 Tracks: 2764 Hits: 16842 = 7.648468e-04 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 1.039680e-04 s
Time CPU Kernel Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 9.894371e-05 s
Time CPU total Cuda2 Events: 9 Tracks: 2764 Hits: 16842 = 6.869330e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.255040e-04 s
Time CPU Kernel Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 1.227856e-04 s
Time CPU total Cuda1 Events: 9 Tracks: 2764 Hits: 16842 = 6.902280e-01 s

Test for Good
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with Good method
Time CPU total Good Events: 9 Tracks: 2764 Hits: 16842 = 4.210472e-04 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 0.000249843 s
Time CPU Kernel OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 0.000298023 s
Time CPU total OpenCl2 Events: 9 Tracks: 2764 Hits: 16842 = 1.062470e+00 s

Test for OpenCL
To import-> Hits: 16842, Tracks: 2764 Events: 9
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 0.000145405 s
Time CPU Kernel OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 0.000190973 s
Time CPU total OpenCl1 Events: 9 Tracks: 2764 Hits: 16842 = 1.047384e+00 s

-----------------------------
Test for Bad
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Bad method
Time CPU total Bad Events: 10 Tracks: 3094 Hits: 18940 = 9.179115e-04 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.061120e-04 s
Time CPU Kernel Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 1.020432e-04 s
Time CPU total Cuda2 Events: 10 Tracks: 3094 Hits: 18940 = 6.900280e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.288640e-04 s
Time CPU Kernel Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 1.251698e-04 s
Time CPU total Cuda1 Events: 10 Tracks: 3094 Hits: 18940 = 6.878471e-01 s

Test for Good
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with Good method
Time CPU total Good Events: 10 Tracks: 3094 Hits: 18940 = 5.080700e-04 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 0.000240763 s
Time CPU Kernel OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 0.000291109 s
Time CPU total OpenCl2 Events: 10 Tracks: 3094 Hits: 18940 = 1.074719e+00 s

Test for OpenCL
To import-> Hits: 18940, Tracks: 3094 Events: 10
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 0.000190838 s
Time CPU Kernel OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 0.000239134 s
Time CPU total OpenCl1 Events: 10 Tracks: 3094 Hits: 18940 = 1.080895e+00 s

-----------------------------
Test for Bad
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Bad method
Time CPU total Bad Events: 11 Tracks: 3613 Hits: 22010 = 1.338959e-03 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.108800e-04 s
Time CPU Kernel Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 1.070499e-04 s
Time CPU total Cuda2 Events: 11 Tracks: 3613 Hits: 22010 = 6.869781e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.366400e-04 s
Time CPU Kernel Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 1.327991e-04 s
Time CPU total Cuda1 Events: 11 Tracks: 3613 Hits: 22010 = 6.888990e-01 s

Test for Good
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with Good method
Time CPU total Good Events: 11 Tracks: 3613 Hits: 22010 = 5.738735e-04 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000204117 s
Time CPU Kernel OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 0.000252962 s
Time CPU total OpenCl2 Events: 11 Tracks: 3613 Hits: 22010 = 1.068395e+00 s

Test for OpenCL
To import-> Hits: 22010, Tracks: 3613 Events: 11
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000208356 s
Time CPU Kernel OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 0.000258923 s
Time CPU total OpenCl1 Events: 11 Tracks: 3613 Hits: 22010 = 1.012256e+00 s

-----------------------------
Test for Bad
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Bad method
Time CPU total Bad Events: 12 Tracks: 3745 Hits: 22930 = 1.372099e-03 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.192000e-04 s
Time CPU Kernel Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 1.161098e-04 s
Time CPU total Cuda2 Events: 12 Tracks: 3745 Hits: 22930 = 6.877890e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.382080e-04 s
Time CPU Kernel Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 1.351833e-04 s
Time CPU total Cuda1 Events: 12 Tracks: 3745 Hits: 22930 = 6.907461e-01 s

Test for Good
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with Good method
Time CPU total Good Events: 12 Tracks: 3745 Hits: 22930 = 6.148815e-04 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000240184 s
Time CPU Kernel OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 0.000288963 s
Time CPU total OpenCl2 Events: 12 Tracks: 3745 Hits: 22930 = 1.131681e+00 s

Test for OpenCL
To import-> Hits: 22930, Tracks: 3745 Events: 12
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000205745 s
Time CPU Kernel OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 0.000256062 s
Time CPU total OpenCl1 Events: 12 Tracks: 3745 Hits: 22930 = 1.042598e+00 s

-----------------------------
Test for Bad
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Bad method
Time CPU total Bad Events: 13 Tracks: 4414 Hits: 27034 = 1.669884e-03 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.248640e-04 s
Time CPU Kernel Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 1.211166e-04 s
Time CPU total Cuda2 Events: 13 Tracks: 4414 Hits: 27034 = 6.877241e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.432320e-04 s
Time CPU Kernel Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 1.389980e-04 s
Time CPU total Cuda1 Events: 13 Tracks: 4414 Hits: 27034 = 6.861429e-01 s

Test for Good
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with Good method
Time CPU total Good Events: 13 Tracks: 4414 Hits: 27034 = 7.128716e-04 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000242994 s
Time CPU Kernel OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 0.000298977 s
Time CPU total OpenCl2 Events: 13 Tracks: 4414 Hits: 27034 = 1.076552e+00 s

Test for OpenCL
To import-> Hits: 27034, Tracks: 4414 Events: 13
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000225068 s
Time CPU Kernel OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 0.000273943 s
Time CPU total OpenCl1 Events: 13 Tracks: 4414 Hits: 27034 = 9.856949e-01 s

-----------------------------
Test for Bad
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Bad method
Time CPU total Bad Events: 14 Tracks: 4587 Hits: 28064 = 1.730919e-03 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.293120e-04 s
Time CPU Kernel Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 1.249313e-04 s
Time CPU total Cuda2 Events: 14 Tracks: 4587 Hits: 28064 = 6.914001e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.462080e-04 s
Time CPU Kernel Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 1.418591e-04 s
Time CPU total Cuda1 Events: 14 Tracks: 4587 Hits: 28064 = 6.876459e-01 s

Test for Good
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with Good method
Time CPU total Good Events: 14 Tracks: 4587 Hits: 28064 = 7.548332e-04 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000201201 s
Time CPU Kernel OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 0.000251055 s
Time CPU total OpenCl2 Events: 14 Tracks: 4587 Hits: 28064 = 1.039442e+00 s

Test for OpenCL
To import-> Hits: 28064, Tracks: 4587 Events: 14
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000255419 s
Time CPU Kernel OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 0.000319958 s
Time CPU total OpenCl1 Events: 14 Tracks: 4587 Hits: 28064 = 9.880829e-01 s

-----------------------------
Test for Bad
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Bad method
Time CPU total Bad Events: 15 Tracks: 5044 Hits: 30895 = 1.784086e-03 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.421760e-04 s
Time CPU Kernel Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 1.368523e-04 s
Time CPU total Cuda2 Events: 15 Tracks: 5044 Hits: 30895 = 6.975389e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.544000e-04 s
Time CPU Kernel Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 1.499653e-04 s
Time CPU total Cuda1 Events: 15 Tracks: 5044 Hits: 30895 = 6.954739e-01 s

Test for Good
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with Good method
Time CPU total Good Events: 15 Tracks: 5044 Hits: 30895 = 8.170605e-04 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000226752 s
Time CPU Kernel OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 0.000275135 s
Time CPU total OpenCl2 Events: 15 Tracks: 5044 Hits: 30895 = 1.150904e+00 s

Test for OpenCL
To import-> Hits: 30895, Tracks: 5044 Events: 15
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000273309 s
Time CPU Kernel OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 0.000326157 s
Time CPU total OpenCl1 Events: 15 Tracks: 5044 Hits: 30895 = 1.034159e+00 s

-----------------------------
Test for Bad
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Bad method
Time CPU total Bad Events: 16 Tracks: 5141 Hits: 31449 = 1.914978e-03 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.352320e-04 s
Time CPU Kernel Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 1.320839e-04 s
Time CPU total Cuda2 Events: 16 Tracks: 5141 Hits: 31449 = 6.847751e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.577600e-04 s
Time CPU Kernel Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 1.528263e-04 s
Time CPU total Cuda1 Events: 16 Tracks: 5141 Hits: 31449 = 6.800201e-01 s

Test for Good
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with Good method
Time CPU total Good Events: 16 Tracks: 5141 Hits: 31449 = 8.261204e-04 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000211459 s
Time CPU Kernel OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 0.000263929 s
Time CPU total OpenCl2 Events: 16 Tracks: 5141 Hits: 31449 = 1.090624e+00 s

Test for OpenCL
To import-> Hits: 31449, Tracks: 5141 Events: 16
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000239162 s
Time CPU Kernel OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 0.000296116 s
Time CPU total OpenCl1 Events: 16 Tracks: 5141 Hits: 31449 = 1.038316e+00 s

-----------------------------
Test for Bad
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Bad method
Time CPU total Bad Events: 17 Tracks: 5353 Hits: 32810 = 1.571178e-03 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.342720e-04 s
Time CPU Kernel Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 1.280308e-04 s
Time CPU total Cuda2 Events: 17 Tracks: 5353 Hits: 32810 = 6.862152e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.619840e-04 s
Time CPU Kernel Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 1.578331e-04 s
Time CPU total Cuda1 Events: 17 Tracks: 5353 Hits: 32810 = 6.856611e-01 s

Test for Good
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with Good method
Time CPU total Good Events: 17 Tracks: 5353 Hits: 32810 = 8.680820e-04 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000204762 s
Time CPU Kernel OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 0.000268936 s
Time CPU total OpenCl2 Events: 17 Tracks: 5353 Hits: 32810 = 1.116864e+00 s

Test for OpenCL
To import-> Hits: 32810, Tracks: 5353 Events: 17
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000199206 s
Time CPU Kernel OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 0.000247955 s
Time CPU total OpenCl1 Events: 17 Tracks: 5353 Hits: 32810 = 1.051871e+00 s

-----------------------------
Test for Bad
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Bad method
Time CPU total Bad Events: 18 Tracks: 5750 Hits: 35226 = 1.802921e-03 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.398400e-04 s
Time CPU Kernel Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 1.358986e-04 s
Time CPU total Cuda2 Events: 18 Tracks: 5750 Hits: 35226 = 6.866100e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.684160e-04 s
Time CPU Kernel Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 1.649857e-04 s
Time CPU total Cuda1 Events: 18 Tracks: 5750 Hits: 35226 = 6.822250e-01 s

Test for Good
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with Good method
Time CPU total Good Events: 18 Tracks: 5750 Hits: 35226 = 9.479523e-04 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000176527 s
Time CPU Kernel OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 0.000226021 s
Time CPU total OpenCl2 Events: 18 Tracks: 5750 Hits: 35226 = 1.139175e+00 s

Test for OpenCL
To import-> Hits: 35226, Tracks: 5750 Events: 18
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000198002 s
Time CPU Kernel OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 0.000241995 s
Time CPU total OpenCl1 Events: 18 Tracks: 5750 Hits: 35226 = 1.011700e+00 s

-----------------------------
Test for Bad
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Bad method
Time CPU total Bad Events: 19 Tracks: 6260 Hits: 38329 = 1.996994e-03 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.387200e-04 s
Time CPU Kernel Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 1.349449e-04 s
Time CPU total Cuda2 Events: 19 Tracks: 6260 Hits: 38329 = 6.535070e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.720320e-04 s
Time CPU Kernel Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 1.690388e-04 s
Time CPU total Cuda1 Events: 19 Tracks: 6260 Hits: 38329 = 6.751668e-01 s

Test for Good
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with Good method
Time CPU total Good Events: 19 Tracks: 6260 Hits: 38329 = 1.013994e-03 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000290624 s
Time CPU Kernel OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 0.000344992 s
Time CPU total OpenCl2 Events: 19 Tracks: 6260 Hits: 38329 = 1.081445e+00 s

Test for OpenCL
To import-> Hits: 38329, Tracks: 6260 Events: 19
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000186026 s
Time CPU Kernel OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 0.000237942 s
Time CPU total OpenCl1 Events: 19 Tracks: 6260 Hits: 38329 = 1.022330e+00 s

-----------------------------
Test for Bad
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Bad method
Time CPU total Bad Events: 20 Tracks: 6560 Hits: 40004 = 2.113819e-03 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.454400e-04 s
Time CPU Kernel Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 1.411438e-04 s
Time CPU total Cuda2 Events: 20 Tracks: 6560 Hits: 40004 = 6.878300e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.708160e-04 s
Time CPU Kernel Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 1.649857e-04 s
Time CPU total Cuda1 Events: 20 Tracks: 6560 Hits: 40004 = 6.874070e-01 s

Test for Good
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with Good method
Time CPU total Good Events: 20 Tracks: 6560 Hits: 40004 = 1.024961e-03 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000192845 s
Time CPU Kernel OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 0.000247955 s
Time CPU total OpenCl2 Events: 20 Tracks: 6560 Hits: 40004 = 1.066142e+00 s

Test for OpenCL
To import-> Hits: 40004, Tracks: 6560 Events: 20
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000274359 s
Time CPU Kernel OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 0.000323057 s
Time CPU total OpenCl1 Events: 20 Tracks: 6560 Hits: 40004 = 1.072522e+00 s

-----------------------------
Test for Bad
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Bad method
Time CPU total Bad Events: 30 Tracks: 9310 Hits: 57158 = 3.371000e-03 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.815680e-04 s
Time CPU Kernel Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 1.759529e-04 s
Time CPU total Cuda2 Events: 30 Tracks: 9310 Hits: 57158 = 6.870139e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.190720e-04 s
Time CPU Kernel Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 2.129078e-04 s
Time CPU total Cuda1 Events: 30 Tracks: 9310 Hits: 57158 = 6.905432e-01 s

Test for Good
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with Good method
Time CPU total Good Events: 30 Tracks: 9310 Hits: 57158 = 1.516104e-03 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000214295 s
Time CPU Kernel OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 0.000272036 s
Time CPU total OpenCl2 Events: 30 Tracks: 9310 Hits: 57158 = 1.079659e+00 s

Test for OpenCL
To import-> Hits: 57158, Tracks: 9310 Events: 30
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000216933 s
Time CPU Kernel OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 0.000256062 s
Time CPU total OpenCl1 Events: 30 Tracks: 9310 Hits: 57158 = 1.026630e+00 s

-----------------------------
Test for Bad
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Bad method
Time CPU total Bad Events: 40 Tracks: 12174 Hits: 75953 = 3.749132e-03 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.071680e-04 s
Time CPU Kernel Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 2.038479e-04 s
Time CPU total Cuda2 Events: 40 Tracks: 12174 Hits: 75953 = 6.913810e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.541440e-04 s
Time CPU Kernel Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 2.501011e-04 s
Time CPU total Cuda1 Events: 40 Tracks: 12174 Hits: 75953 = 6.878419e-01 s

Test for Good
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with Good method
Time CPU total Good Events: 40 Tracks: 12174 Hits: 75953 = 2.004862e-03 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000284243 s
Time CPU Kernel OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 0.000335932 s
Time CPU total OpenCl2 Events: 40 Tracks: 12174 Hits: 75953 = 1.065933e+00 s

Test for OpenCL
To import-> Hits: 75953, Tracks: 12174 Events: 40
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.000236063 s
Time CPU Kernel OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 0.00029397 s
Time CPU total OpenCl1 Events: 40 Tracks: 12174 Hits: 75953 = 1.000753e+00 s

-----------------------------
Test for Bad
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Bad method
Time CPU total Bad Events: 50 Tracks: 15024 Hits: 93663 = 4.992008e-03 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.272320e-04 s
Time CPU Kernel Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 2.238750e-04 s
Time CPU total Cuda2 Events: 50 Tracks: 15024 Hits: 93663 = 6.935580e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.863360e-04 s
Time CPU Kernel Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 2.839565e-04 s
Time CPU total Cuda1 Events: 50 Tracks: 15024 Hits: 93663 = 6.858029e-01 s

Test for Good
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with Good method
Time CPU total Good Events: 50 Tracks: 15024 Hits: 93663 = 2.472878e-03 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000293503 s
Time CPU Kernel OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 0.000458002 s
Time CPU total OpenCl2 Events: 50 Tracks: 15024 Hits: 93663 = 1.116293e+00 s

Test for OpenCL
To import-> Hits: 93663, Tracks: 15024 Events: 50
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.00026532 s
Time CPU Kernel OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 0.000329971 s
Time CPU total OpenCl1 Events: 50 Tracks: 15024 Hits: 93663 = 1.038961e+00 s

-----------------------------
Test for Bad
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Bad method
Time CPU total Bad Events: 60 Tracks: 18356 Hits: 114400 = 6.623030e-03 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.583040e-04 s
Time CPU Kernel Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 2.570152e-04 s
Time CPU total Cuda2 Events: 60 Tracks: 18356 Hits: 114400 = 6.928630e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.169600e-04 s
Time CPU Kernel Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 3.159046e-04 s
Time CPU total Cuda1 Events: 60 Tracks: 18356 Hits: 114400 = 6.903071e-01 s

Test for Good
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with Good method
Time CPU total Good Events: 60 Tracks: 18356 Hits: 114400 = 3.022194e-03 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000521869 s
Time CPU Kernel OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 0.000576019 s
Time CPU total OpenCl2 Events: 60 Tracks: 18356 Hits: 114400 = 1.101869e+00 s

Test for OpenCL
To import-> Hits: 114400, Tracks: 18356 Events: 60
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000278977 s
Time CPU Kernel OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 0.000451088 s
Time CPU total OpenCl1 Events: 60 Tracks: 18356 Hits: 114400 = 1.048755e+00 s

-----------------------------
Test for Bad
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Bad method
Time CPU total Bad Events: 70 Tracks: 21691 Hits: 135212 = 6.495953e-03 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.742720e-04 s
Time CPU Kernel Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 2.739429e-04 s
Time CPU total Cuda2 Events: 70 Tracks: 21691 Hits: 135212 = 6.967139e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.494080e-04 s
Time CPU Kernel Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 3.490448e-04 s
Time CPU total Cuda1 Events: 70 Tracks: 21691 Hits: 135212 = 6.964459e-01 s

Test for Good
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with Good method
Time CPU total Good Events: 70 Tracks: 21691 Hits: 135212 = 3.602028e-03 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.00033881 s
Time CPU Kernel OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 0.000483036 s
Time CPU total OpenCl2 Events: 70 Tracks: 21691 Hits: 135212 = 1.132063e+00 s

Test for OpenCL
To import-> Hits: 135212, Tracks: 21691 Events: 70
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000333436 s
Time CPU Kernel OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 0.000391006 s
Time CPU total OpenCl1 Events: 70 Tracks: 21691 Hits: 135212 = 1.014482e+00 s

-----------------------------
Test for Bad
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Bad method
Time CPU total Bad Events: 80 Tracks: 24967 Hits: 155915 = 7.735014e-03 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.150400e-04 s
Time CPU Kernel Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 3.149509e-04 s
Time CPU total Cuda2 Events: 80 Tracks: 24967 Hits: 155915 = 6.988549e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.911680e-04 s
Time CPU Kernel Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 3.921986e-04 s
Time CPU total Cuda1 Events: 80 Tracks: 24967 Hits: 155915 = 6.934202e-01 s

Test for Good
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with Good method
Time CPU total Good Events: 80 Tracks: 24967 Hits: 155915 = 4.128933e-03 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000342811 s
Time CPU Kernel OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 0.000442982 s
Time CPU total OpenCl2 Events: 80 Tracks: 24967 Hits: 155915 = 1.061707e+00 s

Test for OpenCL
To import-> Hits: 155915, Tracks: 24967 Events: 80
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.00027288 s
Time CPU Kernel OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 0.000438929 s
Time CPU total OpenCl1 Events: 80 Tracks: 24967 Hits: 155915 = 1.010919e+00 s

-----------------------------
Test for Bad
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Bad method
Time CPU total Bad Events: 90 Tracks: 28121 Hits: 175352 = 9.181976e-03 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.365760e-04 s
Time CPU Kernel Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 3.390312e-04 s
Time CPU total Cuda2 Events: 90 Tracks: 28121 Hits: 175352 = 6.939292e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.272960e-04 s
Time CPU Kernel Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 4.301071e-04 s
Time CPU total Cuda1 Events: 90 Tracks: 28121 Hits: 175352 = 6.907351e-01 s

Test for Good
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with Good method
Time CPU total Good Events: 90 Tracks: 28121 Hits: 175352 = 4.647017e-03 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000396331 s
Time CPU Kernel OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 0.000475883 s
Time CPU total OpenCl2 Events: 90 Tracks: 28121 Hits: 175352 = 1.074709e+00 s

Test for OpenCL
To import-> Hits: 175352, Tracks: 28121 Events: 90
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000395959 s
Time CPU Kernel OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 0.000442982 s
Time CPU total OpenCl1 Events: 90 Tracks: 28121 Hits: 175352 = 1.018474e+00 s

-----------------------------
Test for Bad
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Bad method
Time CPU total Bad Events: 100 Tracks: 31506 Hits: 196714 = 1.073503e-02 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.602240e-04 s
Time CPU Kernel Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 3.638268e-04 s
Time CPU total Cuda2 Events: 100 Tracks: 31506 Hits: 196714 = 6.890569e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.682240e-04 s
Time CPU Kernel Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 4.718304e-04 s
Time CPU total Cuda1 Events: 100 Tracks: 31506 Hits: 196714 = 6.904230e-01 s

Test for Good
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with Good method
Time CPU total Good Events: 100 Tracks: 31506 Hits: 196714 = 5.301952e-03 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000443443 s
Time CPU Kernel OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 0.000650167 s
Time CPU total OpenCl2 Events: 100 Tracks: 31506 Hits: 196714 = 1.121790e+00 s

Test for OpenCL
To import-> Hits: 196714, Tracks: 31506 Events: 100
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000467283 s
Time CPU Kernel OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 0.000530958 s
Time CPU total OpenCl1 Events: 100 Tracks: 31506 Hits: 196714 = 1.076693e+00 s

-----------------------------
Test for Bad
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Bad method
Time CPU total Bad Events: 110 Tracks: 34413 Hits: 214801 = 1.203299e-02 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.887040e-04 s
Time CPU Kernel Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 3.929138e-04 s
Time CPU total Cuda2 Events: 110 Tracks: 34413 Hits: 214801 = 6.934781e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.041280e-04 s
Time CPU Kernel Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 5.078316e-04 s
Time CPU total Cuda1 Events: 110 Tracks: 34413 Hits: 214801 = 6.918778e-01 s

Test for Good
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with Good method
Time CPU total Good Events: 110 Tracks: 34413 Hits: 214801 = 5.639076e-03 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000860277 s
Time CPU Kernel OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 0.000936985 s
Time CPU total OpenCl2 Events: 110 Tracks: 34413 Hits: 214801 = 1.063563e+00 s

Test for OpenCL
To import-> Hits: 214801, Tracks: 34413 Events: 110
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000670528 s
Time CPU Kernel OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 0.000869036 s
Time CPU total OpenCl1 Events: 110 Tracks: 34413 Hits: 214801 = 1.080813e+00 s

-----------------------------
Test for Bad
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Bad method
Time CPU total Bad Events: 120 Tracks: 37459 Hits: 233881 = 1.333904e-02 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.184000e-04 s
Time CPU Kernel Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 4.229546e-04 s
Time CPU total Cuda2 Events: 120 Tracks: 37459 Hits: 233881 = 6.869390e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.410560e-04 s
Time CPU Kernel Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 5.459785e-04 s
Time CPU total Cuda1 Events: 120 Tracks: 37459 Hits: 233881 = 6.896310e-01 s

Test for Good
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with Good method
Time CPU total Good Events: 120 Tracks: 37459 Hits: 233881 = 6.199837e-03 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.0004197 s
Time CPU Kernel OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 0.000563145 s
Time CPU total OpenCl2 Events: 120 Tracks: 37459 Hits: 233881 = 1.052835e+00 s

Test for OpenCL
To import-> Hits: 233881, Tracks: 37459 Events: 120
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.000687632 s
Time CPU Kernel OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 0.00088501 s
Time CPU total OpenCl1 Events: 120 Tracks: 37459 Hits: 233881 = 1.018054e+00 s

-----------------------------
Test for Bad
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Bad method
Time CPU total Bad Events: 130 Tracks: 40754 Hits: 254225 = 1.179719e-02 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.456320e-04 s
Time CPU Kernel Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 4.501343e-04 s
Time CPU total Cuda2 Events: 130 Tracks: 40754 Hits: 254225 = 6.978111e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.775040e-04 s
Time CPU Kernel Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 5.838871e-04 s
Time CPU total Cuda1 Events: 130 Tracks: 40754 Hits: 254225 = 6.873689e-01 s

Test for Good
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with Good method
Time CPU total Good Events: 130 Tracks: 40754 Hits: 254225 = 6.792068e-03 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.000630062 s
Time CPU Kernel OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 0.00067997 s
Time CPU total OpenCl2 Events: 130 Tracks: 40754 Hits: 254225 = 1.071327e+00 s

Test for OpenCL
To import-> Hits: 254225, Tracks: 40754 Events: 130
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000419708 s
Time CPU Kernel OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 0.000580072 s
Time CPU total OpenCl1 Events: 130 Tracks: 40754 Hits: 254225 = 1.478523e+00 s

-----------------------------
Test for Bad
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Bad method
Time CPU total Bad Events: 140 Tracks: 44705 Hits: 278857 = 1.314092e-02 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.847680e-04 s
Time CPU Kernel Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 4.918575e-04 s
Time CPU total Cuda2 Events: 140 Tracks: 44705 Hits: 278857 = 7.039740e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.362240e-04 s
Time CPU Kernel Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 6.430149e-04 s
Time CPU total Cuda1 Events: 140 Tracks: 44705 Hits: 278857 = 7.006838e-01 s

Test for Good
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with Good method
Time CPU total Good Events: 140 Tracks: 44705 Hits: 278857 = 7.397890e-03 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000622152 s
Time CPU Kernel OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 0.000669003 s
Time CPU total OpenCl2 Events: 140 Tracks: 44705 Hits: 278857 = 1.107159e+00 s

Test for OpenCL
To import-> Hits: 278857, Tracks: 44705 Events: 140
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.000744071 s
Time CPU Kernel OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 0.00080204 s
Time CPU total OpenCl1 Events: 140 Tracks: 44705 Hits: 278857 = 1.025192e+00 s

-----------------------------
Test for Bad
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Bad method
Time CPU total Bad Events: 150 Tracks: 48106 Hits: 299938 = 1.428199e-02 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.050880e-04 s
Time CPU Kernel Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 5.140305e-04 s
Time CPU total Cuda2 Events: 150 Tracks: 48106 Hits: 299938 = 7.054169e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.660160e-04 s
Time CPU Kernel Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.749630e-04 s
Time CPU total Cuda1 Events: 150 Tracks: 48106 Hits: 299938 = 6.971669e-01 s

Test for Good
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with Good method
Time CPU total Good Events: 150 Tracks: 48106 Hits: 299938 = 7.971048e-03 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.00102149 s
Time CPU Kernel OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 0.00107503 s
Time CPU total OpenCl2 Events: 150 Tracks: 48106 Hits: 299938 = 1.047916e+00 s

Test for OpenCL
To import-> Hits: 299938, Tracks: 48106 Events: 150
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.000776578 s
Time CPU Kernel OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 0.00083518 s
Time CPU total OpenCl1 Events: 150 Tracks: 48106 Hits: 299938 = 1.071338e+00 s

-----------------------------
Test for Bad
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Bad method
Time CPU total Bad Events: 160 Tracks: 51627 Hits: 321407 = 1.579404e-02 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.415680e-04 s
Time CPU Kernel Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 5.509853e-04 s
Time CPU total Cuda2 Events: 160 Tracks: 51627 Hits: 321407 = 6.936440e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.080641e-04 s
Time CPU Kernel Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 7.178783e-04 s
Time CPU total Cuda1 Events: 160 Tracks: 51627 Hits: 321407 = 6.965978e-01 s

Test for Good
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with Good method
Time CPU total Good Events: 160 Tracks: 51627 Hits: 321407 = 8.487940e-03 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000588597 s
Time CPU Kernel OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 0.000651836 s
Time CPU total OpenCl2 Events: 160 Tracks: 51627 Hits: 321407 = 1.084255e+00 s

Test for OpenCL
To import-> Hits: 321407, Tracks: 51627 Events: 160
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.00264576 s
Time CPU Kernel OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 0.00272012 s
Time CPU total OpenCl1 Events: 160 Tracks: 51627 Hits: 321407 = 1.083149e+00 s

-----------------------------
Test for Bad
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Bad method
Time CPU total Bad Events: 170 Tracks: 54445 Hits: 339287 = 1.690316e-02 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.605760e-04 s
Time CPU Kernel Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 5.679131e-04 s
Time CPU total Cuda2 Events: 170 Tracks: 54445 Hits: 339287 = 7.008331e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.381761e-04 s
Time CPU Kernel Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 7.491112e-04 s
Time CPU total Cuda1 Events: 170 Tracks: 54445 Hits: 339287 = 6.956339e-01 s

Test for Good
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with Good method
Time CPU total Good Events: 170 Tracks: 54445 Hits: 339287 = 8.995056e-03 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000728217 s
Time CPU Kernel OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 0.000778913 s
Time CPU total OpenCl2 Events: 170 Tracks: 54445 Hits: 339287 = 1.086778e+00 s

Test for OpenCL
To import-> Hits: 339287, Tracks: 54445 Events: 170
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.000603998 s
Time CPU Kernel OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 0.00065589 s
Time CPU total OpenCl1 Events: 170 Tracks: 54445 Hits: 339287 = 1.081791e+00 s

-----------------------------
Test for Bad
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Bad method
Time CPU total Bad Events: 180 Tracks: 57335 Hits: 357414 = 1.821089e-02 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 5.882561e-04 s
Time CPU Kernel Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.010532e-04 s
Time CPU total Cuda2 Events: 180 Tracks: 57335 Hits: 357414 = 6.987140e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.818560e-04 s
Time CPU Kernel Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 7.929802e-04 s
Time CPU total Cuda1 Events: 180 Tracks: 57335 Hits: 357414 = 6.969290e-01 s

Test for Good
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with Good method
Time CPU total Good Events: 180 Tracks: 57335 Hits: 357414 = 9.009123e-03 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000638041 s
Time CPU Kernel OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 0.000692844 s
Time CPU total OpenCl2 Events: 180 Tracks: 57335 Hits: 357414 = 1.080765e+00 s

Test for OpenCL
To import-> Hits: 357414, Tracks: 57335 Events: 180
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000643788 s
Time CPU Kernel OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 0.000728846 s
Time CPU total OpenCl1 Events: 180 Tracks: 57335 Hits: 357414 = 1.056763e+00 s

-----------------------------
Test for Bad
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Bad method
Time CPU total Bad Events: 190 Tracks: 60572 Hits: 377316 = 1.977706e-02 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.155840e-04 s
Time CPU Kernel Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 6.279945e-04 s
Time CPU total Cuda2 Events: 190 Tracks: 60572 Hits: 377316 = 7.061410e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.099201e-04 s
Time CPU Kernel Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 8.220673e-04 s
Time CPU total Cuda1 Events: 190 Tracks: 60572 Hits: 377316 = 7.006841e-01 s

Test for Good
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with Good method
Time CPU total Good Events: 190 Tracks: 60572 Hits: 377316 = 1.012301e-02 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000746021 s
Time CPU Kernel OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 0.000797033 s
Time CPU total OpenCl2 Events: 190 Tracks: 60572 Hits: 377316 = 1.096842e+00 s

Test for OpenCL
To import-> Hits: 377316, Tracks: 60572 Events: 190
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.00278042 s
Time CPU Kernel OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 0.00300097 s
Time CPU total OpenCl1 Events: 190 Tracks: 60572 Hits: 377316 = 1.057210e+00 s

-----------------------------
Test for Bad
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Bad method
Time CPU total Bad Events: 200 Tracks: 63921 Hits: 398354 = 2.129483e-02 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.546880e-04 s
Time CPU Kernel Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.680489e-04 s
Time CPU total Cuda2 Events: 200 Tracks: 63921 Hits: 398354 = 6.982310e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.610880e-04 s
Time CPU Kernel Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 8.738041e-04 s
Time CPU total Cuda1 Events: 200 Tracks: 63921 Hits: 398354 = 6.934760e-01 s

Test for Good
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with Good method
Time CPU total Good Events: 200 Tracks: 63921 Hits: 398354 = 1.056504e-02 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000763921 s
Time CPU Kernel OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 0.000906944 s
Time CPU total OpenCl2 Events: 200 Tracks: 63921 Hits: 398354 = 1.097111e+00 s

Test for OpenCL
To import-> Hits: 398354, Tracks: 63921 Events: 200
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.000850388 s
Time CPU Kernel OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 0.00102901 s
Time CPU total OpenCl1 Events: 200 Tracks: 63921 Hits: 398354 = 1.051210e+00 s

-----------------------------
Test for Bad
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Bad method
Time CPU total Bad Events: 210 Tracks: 66802 Hits: 416406 = 2.258301e-02 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.726400e-04 s
Time CPU Kernel Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.880760e-04 s
Time CPU total Cuda2 Events: 210 Tracks: 66802 Hits: 416406 = 6.915720e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.012800e-04 s
Time CPU Kernel Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 9.150505e-04 s
Time CPU total Cuda1 Events: 210 Tracks: 66802 Hits: 416406 = 6.990361e-01 s

Test for Good
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with Good method
Time CPU total Good Events: 210 Tracks: 66802 Hits: 416406 = 1.056719e-02 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000788502 s
Time CPU Kernel OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 0.000943184 s
Time CPU total OpenCl2 Events: 210 Tracks: 66802 Hits: 416406 = 1.084544e+00 s

Test for OpenCL
To import-> Hits: 416406, Tracks: 66802 Events: 210
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000677899 s
Time CPU Kernel OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 0.000730991 s
Time CPU total OpenCl1 Events: 210 Tracks: 66802 Hits: 416406 = 1.073256e+00 s

-----------------------------
Test for Bad
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Bad method
Time CPU total Bad Events: 220 Tracks: 69786 Hits: 435132 = 2.397108e-02 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.172481e-04 s
Time CPU Kernel Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.321835e-04 s
Time CPU total Cuda2 Events: 220 Tracks: 69786 Hits: 435132 = 7.099071e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.298560e-04 s
Time CPU Kernel Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 9.448528e-04 s
Time CPU total Cuda1 Events: 220 Tracks: 69786 Hits: 435132 = 7.007639e-01 s

Test for Good
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with Good method
Time CPU total Good Events: 220 Tracks: 69786 Hits: 435132 = 1.107001e-02 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000785437 s
Time CPU Kernel OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 0.000946045 s
Time CPU total OpenCl2 Events: 220 Tracks: 69786 Hits: 435132 = 1.074700e+00 s

Test for OpenCL
To import-> Hits: 435132, Tracks: 69786 Events: 220
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000735083 s
Time CPU Kernel OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 0.000797033 s
Time CPU total OpenCl1 Events: 220 Tracks: 69786 Hits: 435132 = 1.009601e+00 s

-----------------------------
Test for Bad
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Bad method
Time CPU total Bad Events: 230 Tracks: 72547 Hits: 452586 = 2.534986e-02 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.341121e-04 s
Time CPU Kernel Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.500648e-04 s
Time CPU total Cuda2 Events: 230 Tracks: 72547 Hits: 452586 = 7.116880e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.704960e-04 s
Time CPU Kernel Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 9.860992e-04 s
Time CPU total Cuda1 Events: 230 Tracks: 72547 Hits: 452586 = 6.968579e-01 s

Test for Good
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with Good method
Time CPU total Good Events: 230 Tracks: 72547 Hits: 452586 = 1.154304e-02 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.00105341 s
Time CPU Kernel OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 0.00127101 s
Time CPU total OpenCl2 Events: 230 Tracks: 72547 Hits: 452586 = 1.124682e+00 s

Test for OpenCL
To import-> Hits: 452586, Tracks: 72547 Events: 230
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.00211984 s
Time CPU Kernel OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 0.00218296 s
Time CPU total OpenCl1 Events: 230 Tracks: 72547 Hits: 452586 = 1.023179e+00 s

-----------------------------
Test for Bad
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Bad method
Time CPU total Bad Events: 240 Tracks: 76144 Hits: 474872 = 2.705097e-02 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.534720e-04 s
Time CPU Kernel Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.719994e-04 s
Time CPU total Cuda2 Events: 240 Tracks: 76144 Hits: 474872 = 7.076361e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.001024e-03 s
Time CPU Kernel Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 1.019001e-03 s
Time CPU total Cuda1 Events: 240 Tracks: 76144 Hits: 474872 = 6.994190e-01 s

Test for Good
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with Good method
Time CPU total Good Events: 240 Tracks: 76144 Hits: 474872 = 1.183200e-02 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000858007 s
Time CPU Kernel OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 0.000949144 s
Time CPU total OpenCl2 Events: 240 Tracks: 76144 Hits: 474872 = 1.089337e+00 s

Test for OpenCL
To import-> Hits: 474872, Tracks: 76144 Events: 240
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00173748 s
Time CPU Kernel OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 0.00181007 s
Time CPU total OpenCl1 Events: 240 Tracks: 76144 Hits: 474872 = 1.012793e+00 s

-----------------------------
Test for Bad
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Bad method
Time CPU total Bad Events: 250 Tracks: 79172 Hits: 493810 = 2.828312e-02 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.712960e-04 s
Time CPU Kernel Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.898808e-04 s
Time CPU total Cuda2 Events: 250 Tracks: 79172 Hits: 493810 = 7.038000e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.037760e-03 s
Time CPU Kernel Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 1.052856e-03 s
Time CPU total Cuda1 Events: 250 Tracks: 79172 Hits: 493810 = 6.990509e-01 s

Test for Good
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with Good method
Time CPU total Good Events: 250 Tracks: 79172 Hits: 493810 = 1.255202e-02 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00216783 s
Time CPU Kernel OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 0.00230217 s
Time CPU total OpenCl2 Events: 250 Tracks: 79172 Hits: 493810 = 1.109200e+00 s

Test for OpenCL
To import-> Hits: 493810, Tracks: 79172 Events: 250
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.00269369 s
Time CPU Kernel OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 0.00275612 s
Time CPU total OpenCl1 Events: 250 Tracks: 79172 Hits: 493810 = 1.091197e+00 s

-----------------------------
Test for Bad
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Bad method
Time CPU total Bad Events: 260 Tracks: 82444 Hits: 514314 = 2.452493e-02 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.038080e-04 s
Time CPU Kernel Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 8.230209e-04 s
Time CPU total Cuda2 Events: 260 Tracks: 82444 Hits: 514314 = 7.059171e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.072832e-03 s
Time CPU Kernel Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 1.091957e-03 s
Time CPU total Cuda1 Events: 260 Tracks: 82444 Hits: 514314 = 6.940260e-01 s

Test for Good
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with Good method
Time CPU total Good Events: 260 Tracks: 82444 Hits: 514314 = 1.364899e-02 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.000918034 s
Time CPU Kernel OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 0.000971079 s
Time CPU total OpenCl2 Events: 260 Tracks: 82444 Hits: 514314 = 1.070555e+00 s

Test for OpenCL
To import-> Hits: 514314, Tracks: 82444 Events: 260
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.00282196 s
Time CPU Kernel OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 0.00300097 s
Time CPU total OpenCl1 Events: 260 Tracks: 82444 Hits: 514314 = 1.054474e+00 s

-----------------------------
Test for Bad
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Bad method
Time CPU total Bad Events: 270 Tracks: 85661 Hits: 534537 = 2.535200e-02 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.279681e-04 s
Time CPU Kernel Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 8.490086e-04 s
Time CPU total Cuda2 Events: 270 Tracks: 85661 Hits: 534537 = 7.063370e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.117536e-03 s
Time CPU Kernel Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 1.137972e-03 s
Time CPU total Cuda1 Events: 270 Tracks: 85661 Hits: 534537 = 7.005041e-01 s

Test for Good
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with Good method
Time CPU total Good Events: 270 Tracks: 85661 Hits: 534537 = 1.370788e-02 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.00224081 s
Time CPU Kernel OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 0.0023241 s
Time CPU total OpenCl2 Events: 270 Tracks: 85661 Hits: 534537 = 1.089700e+00 s

Test for OpenCL
To import-> Hits: 534537, Tracks: 85661 Events: 270
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.00147229 s
Time CPU Kernel OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 0.00153112 s
Time CPU total OpenCl1 Events: 270 Tracks: 85661 Hits: 534537 = 1.051138e+00 s

-----------------------------
Test for Bad
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Bad method
Time CPU total Bad Events: 280 Tracks: 89234 Hits: 557160 = 2.662516e-02 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.679681e-04 s
Time CPU Kernel Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 8.881092e-04 s
Time CPU total Cuda2 Events: 280 Tracks: 89234 Hits: 557160 = 7.038820e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.164480e-03 s
Time CPU Kernel Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 1.183987e-03 s
Time CPU total Cuda1 Events: 280 Tracks: 89234 Hits: 557160 = 6.920011e-01 s

Test for Good
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with Good method
Time CPU total Good Events: 280 Tracks: 89234 Hits: 557160 = 1.425385e-02 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00127719 s
Time CPU Kernel OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 0.00137401 s
Time CPU total OpenCl2 Events: 280 Tracks: 89234 Hits: 557160 = 1.086258e+00 s

Test for OpenCL
To import-> Hits: 557160, Tracks: 89234 Events: 280
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00212237 s
Time CPU Kernel OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 0.00233698 s
Time CPU total OpenCl1 Events: 280 Tracks: 89234 Hits: 557160 = 1.069946e+00 s

-----------------------------
Test for Bad
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Bad method
Time CPU total Bad Events: 290 Tracks: 92347 Hits: 576611 = 2.762485e-02 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 8.965120e-04 s
Time CPU Kernel Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 9.200573e-04 s
Time CPU total Cuda2 Events: 290 Tracks: 92347 Hits: 576611 = 6.983130e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.195808e-03 s
Time CPU Kernel Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 1.219034e-03 s
Time CPU total Cuda1 Events: 290 Tracks: 92347 Hits: 576611 = 6.994560e-01 s

Test for Good
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with Good method
Time CPU total Good Events: 290 Tracks: 92347 Hits: 576611 = 1.481605e-02 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00127898 s
Time CPU Kernel OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 0.00152493 s
Time CPU total OpenCl2 Events: 290 Tracks: 92347 Hits: 576611 = 1.080700e+00 s

Test for OpenCL
To import-> Hits: 576611, Tracks: 92347 Events: 290
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.00223778 s
Time CPU Kernel OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 0.00245905 s
Time CPU total OpenCl1 Events: 290 Tracks: 92347 Hits: 576611 = 1.040480e+00 s

-----------------------------
Test for Bad
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Bad method
Time CPU total Bad Events: 300 Tracks: 95876 Hits: 598963 = 2.863908e-02 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 2
Time GPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.285760e-04 s
Time CPU Kernel Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 9.529591e-04 s
Time CPU total Cuda2 Events: 300 Tracks: 95876 Hits: 598963 = 7.113080e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Cuda 1
Time GPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.236384e-03 s
Time CPU Kernel Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 1.258850e-03 s
Time CPU total Cuda1 Events: 300 Tracks: 95876 Hits: 598963 = 7.072949e-01 s

Test for Good
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with Good method
Time CPU total Good Events: 300 Tracks: 95876 Hits: 598963 = 1.551700e-02 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 2
Time GPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00119613 s
Time CPU Kernel OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 0.00125599 s
Time CPU total OpenCl2 Events: 300 Tracks: 95876 Hits: 598963 = 1.126591e+00 s

Test for OpenCL
To import-> Hits: 598963, Tracks: 95876 Events: 300
Filter with OpenCL 1
Time GPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.00112979 s
Time CPU Kernel OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 0.00118494 s
Time CPU total OpenCl1 Events: 300 Tracks: 95876 Hits: 598963 = 1.065484e+00 s

-----------------------------
